<?php
//Routes
$app->get('/', function ($request, $response) {

    return $this->view->render($response, '_.twig', [
        'calendar_data' => $request->getAttribute("calendar_data"),
        'calendar_prognosis_data' =>$request->getAttribute("calendar_prognosis_data"),
        'franchise_data'=> $request->getAttribute("franchise_array"),
        'images_main'   => $request->getAttribute("column_logos"),
        'conference_id' => $request->getAttribute("conference_id"),
        'specific_team_data' => $request->getAttribute("specific_team_data"),
        'reference_to_info' => $request->getAttribute("reference_to_info")
        ]);

})->add($mw);

$app->get('/admin', function ($request, $response) {
$tables = ["active_franchises","advanced_team_statistics","goalie_statistics_regular","goalie_statistics_playoff","league_standings",
    "skaters_advanced_statistics_regular","skaters_advanced_statistics_playoff","skaters_basic_statistics_regular","skaters_basic_statistics_playoff","skaters_toi_statistics_regular",
    "skaters_toi_statistics_playoff","team_statistics","team_playoff_statistics"];
$date_time=""; 
    $log_file = fopen(__DIR__ . '/../logs/app.log', "r") or die("Unable to open file!");
    while(!feof($log_file)){
    $line = fgets($log_file);
        for($counter=0; $counter<13; $counter++){
            if (strpos($line,$tables[$counter])) {
            $date_time[$counter] = substr($line, 0, 21);
        }
        }
    }
    fclose($log_file);

    return $this->view->render($response, 'admin.twig', [
        'last_updated_at' => $date_time,
        'table_names'   =>$tables
    ]);

});

$app->get('/admin/{table_name}', function ($request, $response, $args) {
    $tables = ["active_franchises","advanced_team_statistics","goalie_statistics_regular","goalie_statistics_playoff","league_standings",
    "skaters_advanced_statistics_regular","skaters_advanced_statistics_playoff","skaters_basic_statistics_regular","skaters_basic_statistics_playoff","skaters_toi_statistics_regular",
    "skaters_toi_statistics_playoff","team_statistics","team_playoff_statistics"];

    if(in_array($args['table_name'], $tables)){
    
    $this->logger->info("Admin view ".$args['table_name']." table update");
    $data = $request->getAttribute($args['table_name']);
    
    $log_file = fopen(__DIR__ . '/../logs/app.log', "r") or die("Unable to open file!");
    while(!feof($log_file)){
    $line = fgets($log_file);
        for($counter=0; $counter<13; $counter++){
            if (strpos($line,$tables[$counter])) {
            $date_time[$counter] = substr($line, 0, 21);
        }
        }
    }
    fclose($log_file);
    return $this->view->render($response, 'admin.twig', [
        'last_updated_at' => $date_time,
        'table_names'   =>$tables
    ]);

}
else{
    return $this->view->render($response, '404.twig', $args);
}

})->add($admin_table_update);


$app->get('/league_standings', function ($request, $response) {

    return $this->view->render($response, 'specific_team_data.twig', [
        'specific_team_data' => $request->getAttribute("league_standings_array"),
        'league_standings_tooltip_array'  => $request->getAttribute("league_standings_tooltip_array"),
        'league_standings'  => 1,
        'league_standings_tooltip_array' =>$request->getAttribute("league_standings_tooltip_array")
        ]);

})->add($league_standings);

$app->get('/playoff_series_stats', function ($request, $response, $args) {


    return $this->view->render($response, 'playoff_series_stats.twig', [
        'series_stats' => $request->getAttribute("series_stats"),
        ]);
   
})->add($playoff_series_stats);

$app->get('/{id}', function ($request, $response, $args) {


if($args['id']=="league_leaders" || $args['id']=="league_leaders_playoff"){
    if($args['id'] == "league_leaders"){
    $league_leaders = $request->getAttribute("league_leaders_array"); 
    }
    if($args['id'] == "league_leaders_playoff"){
       $league_leaders = $request->getAttribute("league_leaders_playoff_array");  
    }
    
    return $this->view->render($response, 'table_view_template.twig', [
        'league_leaders'    => $league_leaders,
        'league_leaders_tooltip_array'  => $request->getAttribute("league_leaders_tooltip_array"),
        'leaders'           => "true"
    ]);
}
else{ 
$headings = [
    "basic_stats" => "Team Statistics",
    "advanced_stats" => "Advanced team statistics",
    
    "skaters_basic_stats" => "Skaters basic statistics",
    "skaters_advanced_stats" => "Skaters advanced statistics",
    "skaters_toi_stats" => "Skaters TOI statistics",
    "goalies_stats" => "Goalie statistics",

    "skaters_basic_stats_playoffs" => "Skaters basic statistics",
    "skaters_advanced_stats_playoffs" => "Skaters advanced statistics",
    "skaters_toi_stats_playoffs" => "Skaters TOI statistics",
    "goalies_stats_playoffs" => "Goalie statistics"
];
if(array_key_exists($args['id'], $headings)){

	$data = $request->getAttribute($args['id']);
    if(isset($data)){
    $count = count($data[0]);
    }

    // Render index view
    return $this->view->render($response, 'table_view_template.twig', [
        'team_stats' => $data,
        'count'      => $count,
        'player_stats_tooltip_array' =>$request->getAttribute("player_stats_tooltip_array")
    ]);
}
else{
    return $this->view->render($response, '404.twig', $args);
}
}

})->add($team_stats);


$app->get('/team/{team}', function ($request, $response, $args) {

    $team_short_names=["ANA" => "Anaheim Ducks", "ARI"=> "Arizona Coyotes", "BOS"=> "Boston Bruins", "BUF"=> "Buffalo Sabres", "CAR"=> "Carolina Hurricanes", "CBJ"=> "Columbus Blue Jackets", "CGY"=> "Calgary Flames",
"CHI" => "Chicago Blackhawks", "COL" => "Colorado Avalanche", "DAL" => "Dallas Stars", "DET" => "Detroit Red Wings", 
"EDM" => "Edmonton Oilers", "FLA" => "Florida Panthers", "LAK" => "Los Angeles Kings", "MIN" => "Minnesota Wild", "MTL" => "Montreal Canadiens", "NJD" => "New Jersey Devils", "NSH" => "Nashville Predators",
"NYI" => "New York Islanders", "NYR" => "New York Rangers", "OTT" => "Ottawa Senators", "PHI" => "Philadelphia Flyers", 
"PIT" => "Pittsburgh Penguins", "SJS" => "San Jose Sharks", "STL" => "St. Louis Blues", "TBL" => "Tampa Bay Lightning", "TOR" => "Toronto Maple Leafs", "VAN" => "Vancouver Canucks", "WPG" => "Winnipeg Jets", "WSH" => "Washington Capitals"];

   

if(array_key_exists ($args['team'], $team_short_names)){
    return $this->view->render($response, 'specific_team_data.twig', [
        'specific_team_data' => $request->getAttribute("specific_team_data"),
        'heading'   => $team_short_names[$args['team']]
        ]);
}
else{
    return $this->view->render($response, '404.twig', $args);
}
})->add($specific_team_data);


