<?php

// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$mw = function ($request, $response, $next) {

	require __DIR__ . '/../queries/regular_season.php';
	require __DIR__ . '/../scrapping/calendar_scrapper.php';
	require __DIR__ . '/../scrapping/scrapper.php';
	require __DIR__ . '/../scrapping/prognosis.php';

	
 	$reference_to_info=["ANA", "ARI", "BOS", "BUF", "CGY", "CAR", "CHI","COL", "CBJ", "DAL", "DET", "EDM", "FLA", "LAK", "MIN", "MTL", "NSH", "NJD","NYI", "NYR", "OTT", "PHI", "PIT", "SJS", "STL", "TBL", "TOR", "VAN", "WPG", "WSH"];
    $images_array=["../img/ducks.png","../img/arizona.png","../img/boston.png","../img/buffalo.png","../img/flames.png","../img/carolina.png","../img/hawks.png","../img/avalanche.png","../img/columbus.png","../img/dalas.png","../img/detroit.png","../img/oilers.png","../img/panters.png","../img/kings.png","../img/wild.png","../img/montreal.png","../img/predators.png","../img/devils.png","../img/islanders.png","../img/rangers.png","../img/senators.png","../img/flyers.png","../img/penguins.png","../img/sharks.png","../img/blues.png","../img/tampa.png","../img/toronto.png","../img/canucks.png","../img/capitals.png","../img/jets.png"];
    $conference_id_array=["p","p","a","a","p","m","c","c","m","c","a","p","a","p","c","a","c","m","m","m","a","m","m","p","c","a","a","p","m","c"];

  	$request = $request->withAttribute('calendar_data', home_page_calendar());
  	$request = $request->withAttribute('calendar_prognosis_data', prognosis());
	$request = $request->withAttribute('franchise_array', return_regular_season_array('`active_franchises`'));
	$request=$request->withAttribute('column_logos', $images_array);
	$request=$request->withAttribute('conference_id', $conference_id_array);
	$request=$request->withAttribute('reference_to_info', $reference_to_info);

	$response = $next($request, $response);

    return $response;
};

$admin_table_update = function ($request, $response, $next){
	require __DIR__ . '/../scrapping/scraping_data_to_database.php';

    $current_route=$request->getAttribute('routeInfo');
    $current_route=$current_route[2]["table_name"];

	switch ($current_route) {
    case "team_playoff_statistics":
        playoffTeamStatsdata();
        break;
    case "league_standings":
        leagueSummaryData("league_standings");
        break;
	case "team_statistics":
	        leagueSummaryData("team_statistics");
	        break;
	case "advanced_team_statistics":
	        leagueSummaryData("advanced_team_statistics");
        break;
	case "active_franchises":
	        franchize_list_creator();
        break;
    case "skaters_basic_statistics_regular":
	        create_Skaters_Basic_Data("regular", "skater_basic");
        break;
   case "skaters_basic_statistics_playoff":
	        create_Skaters_Basic_Data("playoff", "skater_basic");
        break;
   case "skaters_advanced_statistics_regular":
	        create_Skaters_Basic_Data("regular", "skater_advanced");
        break;
   case "skaters_advanced_statistics_playoff":
	        create_Skaters_Basic_Data("playoff", "skater_advanced");
        break;
   case "skaters_toi_statistics_regular":
	        create_Skaters_Basic_Data("regular", "skater_toi");
        break;
   case "skaters_toi_statistics_playoff":
	        create_Skaters_Basic_Data("playoff", "skater_toi");
        break;
   case "goalie_statistics_regular":
	        create_Skaters_Basic_Data("regular", "goalie");
        break;
   case "goalie_statistics_playoff":
	        create_Skaters_Basic_Data("playoff", "goalie");
        break;
    default:
      	break;
} 
	$response = $next($request, $response);

    return $response;
};


$league_standings = function ($request, $response, $next){

	require __DIR__ . '/../queries/regular_season.php';

	$request = $request->withAttribute('league_standings_array', return_division_array('`league_standings`'));
	$request = $request->withAttribute('league_standings_tooltip_array', league_standing_tooltip_creator());
	$response = $next($request, $response);

    return $response;
};

$playoff_series_stats = function ($request, $response, $next){

	require __DIR__ . '/../queries/regular_season.php';

	$request = $request->withAttribute('series_stats', return_regular_season_array('`team_playoff_statistics`'));
	$response = $next($request, $response);

    return $response;
};


$team_stats = function ($request, $response, $next) {
//tagad japiesledzas datubazei, jaizpilda vaicajums, un japadod skatam masivi ar datiem!

	require __DIR__ . '/../queries/regular_season.php';

	$request = $request->withAttribute('basic_stats', return_regular_season_array('`team_statistics`'));
	$request = $request->withAttribute('advanced_stats', return_regular_season_array('`advanced_team_statistics`'));


	$request = $request->withAttribute('skaters_basic_stats', return_regular_season_array('`skaters_basic_statistics_regular`'));
	$request = $request->withAttribute('skaters_advanced_stats', return_regular_season_array('`skaters_advanced_statistics_regular`'));
	$request = $request->withAttribute('skaters_toi_stats', return_regular_season_array('`skaters_toi_statistics_regular`'));
	$request = $request->withAttribute('goalies_stats', return_regular_season_array('`goalie_statistics_regular`'));


	$request = $request->withAttribute('skaters_basic_stats_playoffs', return_regular_season_array('`skaters_basic_statistics_playoff`'));
	$request = $request->withAttribute('skaters_advanced_stats_playoffs', return_regular_season_array('`skaters_advanced_statistics_playoff`'));
	$request = $request->withAttribute('skaters_toi_stats_playoffs', return_regular_season_array('`skaters_toi_statistics_playoff`'));
	$request = $request->withAttribute('goalies_stats_playoffs', return_regular_season_array('`goalie_statistics_playoff`'));

	
    $request = $request->withAttribute('league_leaders_array', league_leaders_creator("regular"));
    $request = $request->withAttribute('league_leaders_playoff_array', league_leaders_creator("playoff"));
    $request = $request->withAttribute('league_leaders_tooltip_array', leaders_tooltip_creator());
	$request = $request->withAttribute('player_stats_tooltip_array', basic_team_stats_tooltip_creator());
    


    $response = $next($request, $response);

    return $response;
};

$specific_team_data = function ($request, $response, $next) {

	require __DIR__ . '/../queries/regular_season.php';

	$routeInfo = $request->getAttribute('routeInfo');
	$request=$request->withAttribute('specific_team_data', specific_team_data($routeInfo[2]['team']));

	$response = $next($request, $response);

    return $response;
};
