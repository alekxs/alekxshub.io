<?php

//Rezultatu prognozesanas funkcija, kas atgriez masivu ar prognozem, tiek prognozetas tikai sodienas speles
function prognosis(){

//Sakuma iegustu no kalendara funkcijas visas sodienas komandas
$today_games_array = home_page_calendar();
/*
Prognozesanai nepieciesami 6 mainigie
Ligas videji guto vartu skaits spele, ko iegust liga guto vartu skaitu izdalot ar spelu skaitu
Un katras komandas videjo guto un videjo ielaisto vartu skaitu
*/
$GFgPlayoff=$temp_league_G=$temp_league_GP=$GFgHome=$GAgHome=$GFgAway=$GAgAway=0;

//Tiek ieguti sodienas spelu komandu nosaukumi
for($counter=0; $counter<count($today_games_array); $counter++){
	
	$home = $today_games_array[$counter][2];
	$away = $today_games_array[$counter][1];

//Iegustu izslegsanas spelu komandu statistiku no datubazes
	$data=return_regular_season_array('team_playoff_statistics');

//Nepieciesamo datu atlase
	for ( $j=0; $j<16; $j++){
		$temp_league_GP+=$data[$j]['GP'];
		$temp_league_G+=$data[$j]['G'];

		if($data[$j]['Team']==$home){
			$GFgHome = $data[$j]['G']/$data[$j]['GP'];
			$GAgHome = $data[$j]['GA']/$data[$j]['GP'];
		}
		if($data[$j]['Team']==$away){
			$GFgAway = $data[$j]['G']/$data[$j]['GP'];
			$GAgAway = $data[$j]['GA']/$data[$j]['GP'];
		}
	}
//Izslegsanas spelu videji gutie varti spele
$GFgPlayoff=$temp_league_G/$temp_league_GP;

//Aprekinu mi un vi koeficientus, un palielinu iespejas uzvarai majniekiem par 5% un samazinu viesiem par 5%
$mi=$GFgHome*$GAgAway/$GFgPlayoff * 1.05;
$vi=$GAgHome*$GFgAway/$GFgPlayoff / 1.05;

//izsaucu funkciju PrWin, un prognozes rezultatus atgriezu
$prognosis_array[$counter][0]=PrWin($mi, $vi);
$prognosis_array[$counter][1]=PrWin($vi, $mi);

return$prognosis_array;
}

}

//BESSELI funkcija, aizguts no: "http://www.osakac.ac.jp/labs/koeda/tmp/phpexcel/Documentation/API/elementindex_PHPExcel_Calculation.html"

function BESSELI($x, $n) {

	if ((is_numeric($x)) && (is_numeric($n))) {
	    $n    = floor($n);

	    $f_2_PI = 2 * M_PI;

	    if (abs($x) <= 30) {
	        $fTerm = pow($x / 2, $n) / FACT($n);
	        $nK = 1;
	        $fResult = $fTerm;
	        $fSqrX = ($x * $x) / 4;
	        do {
	            $fTerm *= $fSqrX;
	            $fTerm /= ($nK * ($nK + $n));
	            $fResult += $fTerm;
	        } while ((abs($fTerm) > 1e-10) && (++$nK < 100));
	    } else {
	        $fXAbs = abs($x);
	        $fResult = exp($fXAbs) / sqrt($f_2_PI * $fXAbs);
	        if (($n && 1) && ($x < 0)) {
	            $fResult = -$fResult;
	        }
	    }
	    return $fResult;
	}

} 
//Funkcija, kas aprekina faktoriali.
function FACT($factVal) {

	if (is_numeric($factVal)) {
	    if ($factVal < 0) {
	        return -1;
	    }
	    $factLoop = floor($factVal);

	    $factorial = 1;
	    while ($factLoop > 1) {
	        $factorial *= $factLoop--;
	    }
	    return $factorial ;
	}

}    

//Varbutibu rekinasana. Balstoties uz:
/* "Poisson Toolbox 
a review of the application of the
Poisson Probability Distribution in hockey
Copyright Alan Ryder, 2004
Hockey Analytics
www.HockeyAnalytics.com 

27-28 lpp.
*/
function PrWinBy($goals, $mi, $vi){
return EXP(-($mi+$vi)) * pow($mi/$vi,$goals/2) * BESSELI(2*SQRT($mi*$vi),ABS($goals));
}

function PrWin($mi, $vi){
	$PrWin=0;
		for ($h=1 ; $h<100; $h++){
		$PrWin+=PrWinBy($h, $mi, $vi);
		}

	$PrWin += PrWinBy(0, $mi, $vi) * $mi / ($mi + $vi);
	return $PrWin;
}


