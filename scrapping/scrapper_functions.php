<?php

function removeSpacesAndWhitespaces($data_array){
$data_array = preg_replace('/^[ \t]*[\r\n]+/m', '', $data_array);
$data_array = str_replace('   ', '', $data_array);
return $data_array;
};

function StringToArray($data, $player_count, $column_count){
$counter=0;
$separator = "\r\n";
$line = strtok($data, $separator);
for( $i=0; $i<$player_count; $i++){
	for($j=0; $j<$column_count; $j++){
		$line=str_replace( "*", "", $line );
		$ready_array[$counter][$j]=$line;
		$line = strtok( $separator );
	}
	$counter++;
	if(empty($line)){
	break;
	}

}
return $ready_array;
};

