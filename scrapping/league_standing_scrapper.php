<?php

function leagueSummaryData($table){

$league_summary = curl( "http://www.hockey-reference.com/leagues/NHL_2016.html" ); 

$league_summary_string = strip_tags( $league_summary );
$playOffSeries=scrape_between( $league_summary_string,"League Playoff Series","Team Statistics","first" );
$playOffSeries="League Playoff Series".$playOffSeries;
$league_summary_string=str_replace($playOffSeries, "", $league_summary_string );
$league_summary_string = scrape_between( $league_summary_string,"Playoff teams are marked with an asterisk (*)","League Leaders","last" );


//Seit tiek gala ar divizijam 
$league_standing = scrape_between( $league_summary_string,"Atlantic Division","Team Statistics","first" );
$league_standing="Atlantic Division".$league_standing;
$keys=["Atlantic Division", "Metropolitan Division", "Central Division", "Pacific Division" ];


$league_standings_heading=scrape_between( $league_summary_string, "Eastern Conference", "Atlantic Division", "first");
$removable_heading=scrape_between( $league_summary_string, "Western Conference", "Atlantic Division", "first");

//Izdzes ara datus no visiem datiem
$league_summary_string = str_replace( "Eastern Conference".$league_standings_heading, "", $league_summary_string );
$league_summary_string = str_replace( "Western Conference".$league_standings_heading, "", $league_summary_string );

//Tagad ir league_standing mainigais, kura glabajas visi nepieciesamie dati
$league_standing=str_replace( "Eastern Conference".$league_standings_heading, "", $league_standing );
$league_standing=str_replace( "Western Conference".$league_standings_heading, "", $league_standing );

$league_standings_heading = removeSpacesAndWhitespaces($league_standings_heading);

$league_standings_heading = explode("\n", $league_standings_heading);
$league_standings_heading=array_filter($league_standings_heading);
array_unshift($league_standings_heading, "TEAM");

$counter=0;
for( $i=0; $i<sizeof($keys); $i++){
	if($i+1<sizeof($keys)){
	$data=scrape_between( $league_standing,$keys[$i],$keys[$i+1],"first" );
	}
	else{
	$data=scrape_between( $league_standing,$keys[$i]," ","last" );
	}
	$data = removeSpacesAndWhitespaces($data);
	$data = trim($data);
	$data=str_replace( "*", "", $data );
	$ready_standing[$i]=divisionStandingCreator($data, $league_standings_heading, $i);
}

$league_summary_string=str_replace( $league_standing, "", $league_summary_string );




$team_statistics_heading=scrape_between( $league_summary_string, "Team Statistics", "1", "first");
$league_summary_string=str_replace( $team_statistics_heading, "", $league_summary_string );

//Kolonnu nosaukumi
$team_statistics_heading = removeSpacesAndWhitespaces($team_statistics_heading);


$team_statistics_data=scrape_between( $league_summary_string, "Team Statistics", "Advanced Team Statistics", "first");
$league_summary_string=str_replace( "Team Statistics".$team_statistics_data, "", $league_summary_string );
$team_statistics_data = removeSpacesAndWhitespaces($team_statistics_data);

//Izveido masivu ar datiem
$team_statistics_data=str_replace( "*", "", $team_statistics_data );
if($table=="team_statistics"){
$team_statistics_data=StringToArray($team_statistics_data, 30, 27);
//Izveidojam DB un ierakstam datus Jaizpilda 1x Diena
team_statistics_creator($team_statistics_data);
}

$team_statistics_advanced_heading=scrape_between( $league_summary_string, "Advanced Team Statistics", "1", "first");
$league_summary_string=str_replace( $team_statistics_advanced_heading, "", $league_summary_string );

//Kolonnu nosaukumi
$team_statistics_advanced_heading = removeSpacesAndWhitespaces($team_statistics_advanced_heading);

$league_summary_string=$league_summary_string."(*)";
$team_statistics_advanced_data=scrape_between( $league_summary_string, "Advanced Team Statistics", "(*)", "last");
$team_statistics_advanced_data = removeSpacesAndWhitespaces($team_statistics_advanced_data);

//Izveido masivu ar datiem
$team_statistics_advanced_data=str_replace( "*", "", $team_statistics_advanced_data );
if($table=="advanced_team_statistics"){
$team_statistics_advanced_data=StringToArray($team_statistics_advanced_data, 30, 15);
//Izveidojam DB un ierakstam datus Jaizpilda 1x Diena
team_statistics_advanced_creator($team_statistics_advanced_data);
}

//Izveidojam DB un ierakstam datus Jaizpilda 1x Diena
if($table=="league_standings"){
league_standings_creator($ready_standing);
}

}

function playoffTeamStatsdata(){
	//Tiek nolādēta lapa
	$playoff_teams_data = curl( "http://www.hockey-reference.com/playoffs/NHL_2016.html" ); 
	//Tiek noņemti HTML tagi
	$playoff_teams_data = strip_tags( $playoff_teams_data );
	//Tiek atlasīti nepieciešamie dati no HTML lapas satura
	$playoff_teams_data=scrape_between( $playoff_teams_data,"Team Statistics","Overtime Goals","first" );
	//Tiek izņemtas atstarpes un jaunas rindas
	$playoff_teams_data = removeSpacesAndWhitespaces($playoff_teams_data);
	//Tiek izveidots masīvs no nepieciešamajiem datiem
	$playoff_teams_data=StringToArray($playoff_teams_data, 17, 12);
	//Tiek izņemti virsraksti no masīva
	array_shift($playoff_teams_data);
	//Tagad jāizveido DB
	team_statistics_playoff_creator($playoff_teams_data);
	//print_r($playoff_teams_data);

}


function divisionStandingCreator( $divison_data, $keys, $index){
$keys_id=["ad","md","cd","pd"];
$counter=0;
$separator = "\r\n";
$line = strtok($divison_data, $separator);
	while ($line !== false) {
		for($i=0; $i<sizeof($keys); $i++){
			$return_array[$counter][$i]=$line;
			$line = strtok( $separator );
		}
					$return_array[$counter][13]=$keys_id[$index];
	$counter++;
	}

return $return_array;
}