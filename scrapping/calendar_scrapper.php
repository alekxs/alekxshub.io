<?php
//Nolade kalendaaru, + tekosas speles
function home_page_calendar(){

require_once __DIR__ . '/../scrapping/scrapper_functions.php';

$current_date=date("Y/m/d");
$calendar=curl( "http://www.hockey-reference.com/boxscores/$current_date" );
$pos = strpos($calendar, "Sorry, there are no games in the box score database for this date.");

if($pos===false){

$calendar=scrape_between( $calendar,"<tbody>","</tbody>","first" );
$calendar = strip_tags( $calendar );
$calendar=removeSpacesAndWhitespaces($calendar);
$ready_calendar_data=StringToArray($calendar, 3, 3);

for($counter=0; $counter<count($ready_calendar_data);$counter++){
unset($ready_calendar_data[$counter][3]);
}

return $ready_calendar_data;


}
}
