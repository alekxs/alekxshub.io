<?php
//Dotajā failā tiek veidotas 3 lielas statistikas tabulas par spēlētājiem un 1 mazāka par vārtsargiem
function create_Skaters_Basic_Data($season_statuss, $position){
//Tiek ieguta vajadziga lapa
if($season_statuss=="regular"){
$skaters_basic=curl( "http://www.hockey-reference.com/leagues/NHL_2016_skaters.html" );  
$skaters_advanced=curl( "http://www.hockey-reference.com/leagues/NHL_2016_skaters-advanced.html" );
$skaters_TOI=curl( "http://www.hockey-reference.com/leagues/NHL_2016_skaters-time-on-ice.html" );
$goalies=curl( "http://www.hockey-reference.com/leagues/NHL_2016_goalies.html" );
}

if($season_statuss=="playoff"){
$skaters_basic=curl( "http://www.hockey-reference.com/playoffs/NHL_2016_skaters.html" );  
$skaters_advanced=curl( "http://www.hockey-reference.com/playoffs/NHL_2016_skaters-advanced.html" );
$skaters_TOI=curl( "http://www.hockey-reference.com/playoffs/NHL_2016_skaters-time-on-ice.html" );
$goalies=curl( "http://www.hockey-reference.com/playoffs/NHL_2016_goalies.html" );
}

//Tiek aizpildīts tukšais lauks tabulās, lai vieglāka datu apstrāde
$skaters_basic=str_replace( "<td align=\"center\" ></td>","<td align=\"center\" >x</td>",$skaters_basic );
$skaters_basic=str_replace( "<td align=\"right\" ></td>","<td align=\"right\" >x</td>",$skaters_basic );
$skaters_advanced=str_replace( "<td align=\"right\" ></td>","<td align=\"right\" >x</td>",$skaters_advanced );
$skaters_advanced=str_replace( "<td align=\"center\" ></td>","<td align=\"center\" >24</td>",$skaters_advanced );
$skaters_advanced=str_replace( "<td align=\"\" ></td>","<td align=\"\" >x</td>",$skaters_advanced );
$skaters_TOI=str_replace( "<td align=\"right\" ></td>","<td align=\"right\" >x</td>",$skaters_TOI );
$goalies=str_replace( "<td align=\"right\" ></td>","<td align=\"right\" >x</td>",$goalies );

//Noņemti HTML tagi
$skaters_basic = strip_tags( $skaters_basic );
$skaters_advanced = strip_tags( $skaters_advanced );
$skaters_TOI = strip_tags( $skaters_TOI );
$goalies = strip_tags( $goalies );

//atsijati vajadzigie dati no liekā
$skaters_basic=scrape_between( $skaters_basic,"Ice Time","(function () {var sr_js_file","last" );
$skaters_advanced=scrape_between( $skaters_advanced,"Thru%","(function () {var sr_js_file","last" );
$skaters_TOI=scrape_between( $skaters_TOI,"GA/60","(function () {var sr_js_file","last" );
$goalies=scrape_between( $goalies,"PIM","(function () {var sr_js_file","last" );

//Noņemtas atstarpes un jaunas rindas
$skaters_basic=removeSpacesAndWhitespaces($skaters_basic);
$skaters_advanced=removeSpacesAndWhitespaces($skaters_advanced);
$skaters_TOI=removeSpacesAndWhitespaces($skaters_TOI);
$goalies=removeSpacesAndWhitespaces($goalies);

//Tiek atmesti kolonnu nosaukumi, tie tiks manuali vadīti pie datubāzes izveides
$heading_basic=scrape_between( $skaters_basic,"Rk","FO%","first");
$heading_basic="  Rk".$heading_basic."FO%";
$heading_advanced=scrape_between( $skaters_advanced,"Corsi (EV)","Thru%","first");
$heading_advanced="  Corsi (EV)".$heading_advanced."Thru%";
$heading_TOI=scrape_between( $skaters_TOI,"TOI","GA/60","first");
$heading_TOI="  TOI".$heading_TOI."GA/60";
$heading_goalies=scrape_between( $goalies,"Goalie Stats","PIM","first");
$heading_goalies="  Goalie Stats".$heading_goalies."PIM";

//Izņem ārā kolonnu nosaukumus no datiem + papildu tabulu kolonnas, kas ir scrapotajā lapā
$skaters_basic=str_replace( $heading_basic, "", $skaters_basic );
$skaters_advanced=str_replace( $heading_advanced, "", $skaters_advanced );
$skaters_TOI=str_replace( $heading_TOI, "", $skaters_TOI );
$goalies=str_replace( $heading_goalies, "", $goalies );
$removable_string=scrape_between( $skaters_basic,"Scoring","Ice Time","first");
$removable_string="  Scoring".$removable_string."Ice Time";
$skaters_basic=str_replace( $removable_string, "", $skaters_basic );
$removable_string=scrape_between( $skaters_TOI,"Even Strength","GP","first");
$removable_string="  Even Strength".$removable_string."GP";
$skaters_TOI=str_replace( $removable_string, "", $skaters_TOI );



if($position=="skater_basic"){
	$data_array_basic=StringToArray($skaters_basic, 1000, 27);
	skaters_basic_creator($data_array_basic,$season_statuss);
}


if($position=="skater_advanced"){
	$data_array_advanced=StringToArray($skaters_advanced, 2000, 28);
	skaters_advanced_creator($data_array_advanced,$season_statuss);	
}


if($position=="skater_toi"){
	$data_array_TOI=StringToArray($skaters_TOI, 1000, 18);
	skaters_toi_creator($data_array_TOI,$season_statuss);	
}

if($position=="goalie"){
	if($season_statuss == 'regular'){
		$data_array_goalie=StringToArray($goalies, 100, 25);
		goalie_statistics__creator($data_array_goalie);
	}
	else{
		$data_array_goalie=StringToArray($goalies, 100, 24);
		goalie_statistics__creator_playoff($data_array_goalie);
	}
}


}

