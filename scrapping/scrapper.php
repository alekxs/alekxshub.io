<?php
//Ar so funkciju nolade HTML saturu 
function curl($url) {
// 
$options = Array(
    CURLOPT_RETURNTRANSFER => TRUE,  // Setting cURL's option to return the webpage data
    CURLOPT_FOLLOWLOCATION => TRUE,  // Setting cURL to follow 'location' HTTP headers
    CURLOPT_AUTOREFERER => TRUE, // Automatically set the referer where following 'location' HTTP headers
    CURLOPT_CONNECTTIMEOUT => 120,   // Setting the amount of time (in seconds) before the request times out
    CURLOPT_TIMEOUT => 120,  // Setting the maximum amount of time for cURL to execute queries
    CURLOPT_MAXREDIRS => 10, // Setting the maximum number of redirections to follow
    CURLOPT_USERAGENT => "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1a2pre) Gecko/2008073000 Shredder/3.0a2pre ThunderBrowse/3.2.1.8",  // Setting the useragent
    CURLOPT_URL => $url, // Setting cURL's URL option with the $url variable passed into the function
);
 
$ch = curl_init();  
curl_setopt_array($ch, $options);   // Setting cURL's options 
$data = curl_exec($ch); // Executing the cURL request 
curl_close($ch);    
return $data;  
};

function scrape_between($data, $start, $end, $way){

$data = stristr($data, $start); // sak meklet no $start 

$data = substr($data, strlen($start));  
if($way=="last"){ 
$stop = strrpos($data, $end);   // last occurrence 
}
if($way=="first"){ 
$stop = strpos($data, $end);    // first occurrence 
}

$data = substr($data, 0, $stop);    // mekle no dokumenta beigam

return $data;   // atgriez atrastos datus!
              
}