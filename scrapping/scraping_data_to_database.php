<?php

require_once __DIR__ . '/../scrapping/scrapper.php';
require_once __DIR__ . '/../scrapping/scrapper_functions.php';

require_once __DIR__ . '/../scrapping/league_standing_scrapper.php';
require_once __DIR__ . '/../scrapping/skaters_goalies_statistics.php';
require_once __DIR__ . '/../scrapping/active_franchise_scrapper.php';

require_once __DIR__ . '/../database/league_standings_database.php';
require_once __DIR__ . '/../database/team_statistics_database.php';
require_once __DIR__ . '/../database/team_statistics_playoff_database.php';
require_once __DIR__ . '/../database/team_statistics_advanced_database.php';
require_once __DIR__ . '/../database/skaters_basic_database.php';
require_once __DIR__ . '/../database/skaters_advanced_database.php';
require_once __DIR__ . '/../database/skaters_toi_database.php';
require_once __DIR__ . '/../database/goalies_database.php';
require_once __DIR__ . '/../database/goalies_database_playoff.php';
require_once __DIR__ . '/../database/league_franchise_database.php';



