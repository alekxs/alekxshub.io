<?php

/* index.twig */
class __TwigTemplate_44bd9a82d7908a3e259c4292b37fb400f920b3c7021ff45a0ab02ef1de36a075 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <title>Kvalifikācijas darbs</title>
        <meta charset=\"utf-8\"/>
        <link href='//fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
        <link rel=\"stylesheet\" type=\"text/less\" href=\"../css/front.less\">
        <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\" type=\"text/javascript\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/less.js/2.5.3/less.min.js\"></script>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>
        <script src=\"../js/functions.js\"></script>
    </head>
    <body> 
        <div id=\"container\">
            <div id=\"header\">
                <div class=\"team-selection\">
                    <p>Choose your favourite team: </p>
                         <select >
                              <option value=\"none\">Hide favourite team</option>
                              <option value=\"Anaheim\">Anaheim Ducks</option>
                              <option value=\"Arizona\">Arizona Coyotes</option>
                              <option value=\"Boston\">Boston Bruins</option>
                              <option value=\"Buffalo\">Buffalo Sabres</option>
                              <option value=\"Calgary\">Calgary Flames</option>
                              <option value=\"Carolina\">Carolina Hurricanes</option>
                              <option value=\"Chicago\">Chicago Blackhawks</option>
                              <option value=\"Colorado\">Colorado Avalanche</option>
                              <option value=\"Columbus\">Columbus Blue Jackets</option>
                              <option value=\"Dallas\">Dallas Stars</option>
                              <option value=\"Detroit\">Detroit Red Wings</option>
                              <option value=\"Edmonton\">Edmonton Oilers</option>
                              <option value=\"Florida\">Florida Panthers</option>
                              <option value=\"Kings\">Los Angeles Kings</option>
                              <option value=\"Minesota\">Minnesota Wild</option>
                              <option value=\"Montreal\">Montreal Canadiens</option>
                              <option value=\"Nashvile\">Nashville Predators</option>
                              <option value=\"Jersey\">New Jersey Devils</option>
                              <option value=\"Islanders\">New York Islanders</option>
                              <option value=\"Rangers\">New York Rangers</option>
                              <option value=\"Ottawa\">Ottawa Senators</option>
                              <option value=\"Philadelphia\">Philadelphia Flyers</option>
                              <option value=\"Pitsburgh\">Pittsburgh Penguins</option>
                              <option value=\"Sharks\">San Jose Sharks</option>
                              <option value=\"Blues\">St Louis Blues</option>
                              <option value=\"Lightning\">Tampa Bay Lightning</option>
                              <option value=\"Toronto\">Toronto Maple Leafs</option>
                              <option value=\"Vancouver\">Vancouver Canucks</option>
                              <option value=\"Washington\">Washington Capitals</option>
                              <option value=\"Winnipeg\">Winnipeg Jets</option>
                        </select> 
                </div>
                    <div id=\"nav-bar\">
                        <div class=\"left\">
                            <div id=\"logo\"></div>
                        </div>
                            <div class=\"right\">
                                <ul class=\"nav-bar\">
                                  <li><a class=\"active\" href=\"#home\">Home</a></li>
                                  <li><a href=\"#news\">News</a></li>
                                  <li><a href=\"#contact\">Contact</a></li>
                                  <li><a href=\"#about\">About</a></li>
                                </ul>
                            </div>
                    </div>
                </div>
                    
                <div class=\"left-columns\">
                    <div id=\"team-data\">
                      <div class=\"team-logo\"></div>
                    </div>
                </div>
                
                    <div class=\"data\">
                        
                        <div class=\"calendar\">
                            <h1>Calendar</h1>
                            <p>Info</p>
                        </div>
                            <div class=\"leaders\">
                                <h1>Leaders</h1>
                                <p>info</p>
                            </div>

                            <div class=\"leaders\">
                                <h1>Other data</h1>
                                <p>info</p>
                            </div>
                    </div>

        </div>
     
";
        // line 91
        $this->displayBlock('main', $context, $blocks);
        // line 93
        echo "<!--
     
  
        <?php if (isset(\$name)) : ?>
            <h2>Hello <?= htmlspecialchars(\$name); ?>!</h2>
        <?php else: ?>
            <p>Try <a href=\"/SlimFramework\">/SlimFramework</a>
        <?php endif; ?>
     
        -->
    </body>
</html>
";
    }

    // line 91
    public function block_main($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function getDebugInfo()
    {
        return array (  130 => 91,  114 => 93,  112 => 91,  20 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         <title>Kvalifikācijas darbs</title>*/
/*         <meta charset="utf-8"/>*/
/*         <link href='//fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>*/
/*         <link rel="stylesheet" type="text/less" href="../css/front.less">*/
/*         <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>*/
/*         <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.5.3/less.min.js"></script>*/
/*         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>*/
/*         <script src="../js/functions.js"></script>*/
/*     </head>*/
/*     <body> */
/*         <div id="container">*/
/*             <div id="header">*/
/*                 <div class="team-selection">*/
/*                     <p>Choose your favourite team: </p>*/
/*                          <select >*/
/*                               <option value="none">Hide favourite team</option>*/
/*                               <option value="Anaheim">Anaheim Ducks</option>*/
/*                               <option value="Arizona">Arizona Coyotes</option>*/
/*                               <option value="Boston">Boston Bruins</option>*/
/*                               <option value="Buffalo">Buffalo Sabres</option>*/
/*                               <option value="Calgary">Calgary Flames</option>*/
/*                               <option value="Carolina">Carolina Hurricanes</option>*/
/*                               <option value="Chicago">Chicago Blackhawks</option>*/
/*                               <option value="Colorado">Colorado Avalanche</option>*/
/*                               <option value="Columbus">Columbus Blue Jackets</option>*/
/*                               <option value="Dallas">Dallas Stars</option>*/
/*                               <option value="Detroit">Detroit Red Wings</option>*/
/*                               <option value="Edmonton">Edmonton Oilers</option>*/
/*                               <option value="Florida">Florida Panthers</option>*/
/*                               <option value="Kings">Los Angeles Kings</option>*/
/*                               <option value="Minesota">Minnesota Wild</option>*/
/*                               <option value="Montreal">Montreal Canadiens</option>*/
/*                               <option value="Nashvile">Nashville Predators</option>*/
/*                               <option value="Jersey">New Jersey Devils</option>*/
/*                               <option value="Islanders">New York Islanders</option>*/
/*                               <option value="Rangers">New York Rangers</option>*/
/*                               <option value="Ottawa">Ottawa Senators</option>*/
/*                               <option value="Philadelphia">Philadelphia Flyers</option>*/
/*                               <option value="Pitsburgh">Pittsburgh Penguins</option>*/
/*                               <option value="Sharks">San Jose Sharks</option>*/
/*                               <option value="Blues">St Louis Blues</option>*/
/*                               <option value="Lightning">Tampa Bay Lightning</option>*/
/*                               <option value="Toronto">Toronto Maple Leafs</option>*/
/*                               <option value="Vancouver">Vancouver Canucks</option>*/
/*                               <option value="Washington">Washington Capitals</option>*/
/*                               <option value="Winnipeg">Winnipeg Jets</option>*/
/*                         </select> */
/*                 </div>*/
/*                     <div id="nav-bar">*/
/*                         <div class="left">*/
/*                             <div id="logo"></div>*/
/*                         </div>*/
/*                             <div class="right">*/
/*                                 <ul class="nav-bar">*/
/*                                   <li><a class="active" href="#home">Home</a></li>*/
/*                                   <li><a href="#news">News</a></li>*/
/*                                   <li><a href="#contact">Contact</a></li>*/
/*                                   <li><a href="#about">About</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                     </div>*/
/*                 </div>*/
/*                     */
/*                 <div class="left-columns">*/
/*                     <div id="team-data">*/
/*                       <div class="team-logo"></div>*/
/*                     </div>*/
/*                 </div>*/
/*                 */
/*                     <div class="data">*/
/*                         */
/*                         <div class="calendar">*/
/*                             <h1>Calendar</h1>*/
/*                             <p>Info</p>*/
/*                         </div>*/
/*                             <div class="leaders">*/
/*                                 <h1>Leaders</h1>*/
/*                                 <p>info</p>*/
/*                             </div>*/
/* */
/*                             <div class="leaders">*/
/*                                 <h1>Other data</h1>*/
/*                                 <p>info</p>*/
/*                             </div>*/
/*                     </div>*/
/* */
/*         </div>*/
/*      */
/* {% block main %}*/
/* {% endblock %}*/
/* <!--*/
/*      */
/*   */
/*         <?php if (isset($name)) : ?>*/
/*             <h2>Hello <?= htmlspecialchars($name); ?>!</h2>*/
/*         <?php else: ?>*/
/*             <p>Try <a href="/SlimFramework">/SlimFramework</a>*/
/*         <?php endif; ?>*/
/*      */
/*         -->*/
/*     </body>*/
/* </html>*/
/* */
