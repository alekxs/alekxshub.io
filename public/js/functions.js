
function show_calendar(){
  
  var height = $(window).scrollTop();
  height+=150;
  $('.calendar').css("margin-top",height+"px");
  $('.calendar').css("display","block");
}

function hide_calendar(){
  $('.calendar').css("display","none");
}
$(window).load(function(){
   var pathname = window.location.pathname;
$("#cssmenu li").each(function() {
    var current = ($(this).find('a').attr('href'));
    if($(this).find('a').attr('href')==pathname){
      if(pathname!="/"){
       $(this).parent().parent().addClass( "active" );
        $(this).parent().parent().parent().parent().addClass( "active" );
      }
      else{
        $(this).addClass( "active" )
      }
    }
    else $(this).removeClass( "active" );  
});
});
//Visa logika, kad dokuments ir ieladets
 $(document).ready(function(){
  search_data();
  page_header();
  mainmenu();
  $('.table_reset').hide();
        $('.table_reset').click(function() {
            location.reload(); 
        });
  var pathname = window.location.pathname;
  if(pathname!="/"){
$('.calendar_icon').css({display: "none"});
$('.sort_buttons').css({display: "none"});

  }
  $('.header_logo_top').hover(function() {
        $(this).addClass('transition');
 
    }, function() {
        $(this).removeClass('transition');
    });

if(pathname.indexOf('/admin') > -1){
$('#images').css({display: "none"});
$('#cssmenu').css({display: "none"});
}
{
if(pathname.indexOf('team') > -1){
$('#maincontainer_big_table').hide();
var res = pathname;
res=res.replace("/team/", "");

 $('#header').css('background', 'linear-gradient(to right, '+background_colors[res]+')');
 $('.specific_team_heading').css('color', 'white');
 
}
if(pathname == "/league_standings"){
  $('#maincontainer_big_table').hide();
}
$('.sort_buttons_specific_team button').click( function() {

    if($(this).index() > 2 && pathname != "/league_standings"){
   
    $('#childcontainer_big_table').css('width', 'auto');
  }

  $('#maincontainer_big_table').show();
  $('#maincontainer_big_table table').hide().eq( $(this).index() ).show();
  $('button').removeClass('active');
  $(this).addClass('active');
 
  return false;
} );
}

});

 function mainmenu(){

$(" #nav ul ").css({display: "none"}); // Opera Fix
$(" #nav li").hover(function(){
    $(this).find('ul:first').css({visibility: "visible",display: "none"}).show(400);
    },function(){
    $(this).find('ul:first').css({visibility: "hidden"});
    });

}
//konkreta skata virsraksts
function page_header(){
  var pathname = window.location.pathname;

  var arr = { "/league_leaders": "League Leaders", "/": "Active Franchises", "/league_standings": "League Standings", "/basic_stats": "Team Basic Statistics", "/advanced_stats": "Team Advanced Statistics"
  , "/skaters_basic_stats": "Skater Basic Statistics", "/skaters_basic_stats_playoffs": "Skater Basic Statistics","/skaters_advanced_stats": "Skater Advanced Statistics",
  "/skaters_advanced_stats_playoffs": "Skater Advanced Statistics", "/skaters_toi_stats": "Skater Time on Ice Statistics",
  "/skaters_toi_stats_playoffs": "Skater Time on Ice Statistics", "/goalies_stats": "Goalie Statistics", "/goalies_stats_playoffs": "Goalie Statistics" ,"/playoff_series_stats": "Playoff Team Statistics" }; 
 
   $(".selected_header").html( arr[pathname] );
}



$(function(){

  $('#childcontainer_big_table').scroll(function(){
    var height = $(this).scrollTop();
    $('.fixed_header').css('top', height);
  });

//Izsaucu mixitup biblioteku
  $('#Container').mixItUp({
    load: {
      sort: 'random',
      filter: '.category-all'
    }
  });
  $('#leaders_mixing').mixItUp({
  load: {
    sort: 'random',
    filter: '.category-all'
  }
});

});
//Meklesana tabulaas, aizguuta ideja no: "https://codepen.io/chriscoyier/pen/tIuBL"

function search_data() {
  'use strict';

  var LightTableFilter = (function(Arr) {

    var _input;

    function _onInputEvent(e) {
     
      _input = e.target;
     
      var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
      Arr.forEach.call(tables, function(table) {
        Arr.forEach.call(table.tBodies, function(tbody) {
          Arr.forEach.call(tbody.rows, _filter);
        });
      });
    }

    function _filter(row) {
    $('.info_header').hide();
    if( _input.value == ''){
    $('.info_header').show();
    }
      var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
      row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
    }

    return {
      init: function() {
        var inputs = document.getElementsByClassName('light-table-filter');
        Arr.forEach.call(inputs, function(input) {
          input.oninput = _onInputEvent;
        });
      }
    };
  })(Array.prototype);

  document.addEventListener('readystatechange', function() {
    if (document.readyState === 'complete') {
      LightTableFilter.init();
    }
  });

};

//Konkretas komandas nav-joslas kraasas

 var background_colors = { "ANA" : "#000000, #91764B,#EF5225", 
  "ARI" : "#841F27, #000000,#EFE1C6", 
  "BOS" : "#000000, #FFC422", 
  "BUF" :"#002E62, #FDBB2F,#AEB6B9", 
  "CGY" : "#E03A3E, #FFC758,#000000", 
  "CAR" : "#E03A3E, #000000,#8E8E90", 
  "CHI" : "#E3263A, #000000", 
  "COL" : "#8B2942, #01548A,#000000,#A9B0B8", 
  "CBJ" : "#00285C, #E03A3E, #A9B0B8", 
  "DAL" : "#006A4E, #000000, #C0C0C0", 
  "DET" : "#EC1F26, white", 
  "EDM" : "#003777, #E66A20", 
  "FLA" : "#C8213F, #002E5F, #D59C05", 
  "LAK" : "#000000, #AFB7BA", 
  "MIN" : "#025736, #BF2B37, #EFB410, #EEE3C7",
  "MTL" : "#BF2F38, #213770", 
  "NSH" : "#FDBB2F, #002E62", 
  "NJD" : "#E03A3E, #000000", 
  "NYI" : "#00529B, #F57D31", 
  "NYR" : "#0161AB, #E6393F", 
  "OTT" : "#E4173E, #000000, #D69F0F", 
  "PHI" : "#F47940, #000000",
  "PIT" : "#000000, #D1BD80", 
  "SJS" :"#05535D, #F38F20, #000000", 
  "STL" : "#0546A0, #FFC325, #101F48", 
  "TBL" : "#013E7D, #000000, #C0C0C0", 
  "TOR" : "#003777, white", 
  "VAN" : "#07346F, #047A4A, #A8A9AD", 
  "WSH" : "#CF132B, #00214E, #000000", 
  "WPG" : "#002E62, #0168AB, #A8A9AD"};

  