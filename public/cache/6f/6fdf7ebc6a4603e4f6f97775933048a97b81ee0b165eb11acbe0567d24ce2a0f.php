<?php

/* specific_team_data.twig */
class __TwigTemplate_b98d3afe912e67b142a87fff13d3991a6dc6e70062886fd9d774d934778325b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("_.twig", "specific_team_data.twig", 1);
        $this->blocks = array(
            'specific_team_data' => array($this, 'block_specific_team_data'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_specific_team_data($context, array $blocks = array())
    {
        // line 4
        echo "<h1 class=\"specific_team_heading\">";
        echo twig_escape_filter($this->env, (isset($context["heading"]) ? $context["heading"] : null), "html", null, true);
        echo "</h1>
";
    }

    // line 6
    public function block_main($context, array $blocks = array())
    {
        // line 7
        echo "<div class=\"selected_team_info\" id=\"specific_team_data\">
    <div class=\"sort_buttons_specific_team\">
    ";
        // line 9
        if (((isset($context["league_standings"]) ? $context["league_standings"] : null) != 1)) {
            // line 10
            echo "      <button>PLAYER BASIC STATISTICS</button>
      <button>PLAYER ADVANCED STATISTICS</button>
      <button>TOI STATISTICS</button>
      <button>GOALIE STATISTICS</button>
      <button>TEAM STATISTICS</button>
      <button>TEAM ADVANCED STATISTICS</button>
     ";
        }
        // line 17
        echo "  ";
        if (((isset($context["league_standings"]) ? $context["league_standings"] : null) == 1)) {
            // line 18
            echo "      <button>ATLANTIC DIVISION</button>
      <button>PACIFIC DIVISION</button>
      <button>METROPOLIAN DIVISION</button>
      <button>CENTRAL DIVISION</button>
  ";
        }
        // line 23
        echo "    </div>
    <div id=\"maincontainer_big_table\">

      <button class=\"table_reset\">Reset sorting</button>
      <input type=\"search\" class=\"light-table-filter fixed_header\" data-table=\"order-table\" placeholder=\"Search...\">
      ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 29
            echo "        <table class=\"team_stats_list order-table sortable\">
        <thead class=\"fixed_header\">
        ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute($this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), $context["data"], array(), "array"), 0, array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["indexs"]) {
                // line 32
                echo "          ";
                if ((((($context["indexs"] != "Tm") && ($context["indexs"] != "Rk")) && ($context["indexs"] != "ID")) && ($context["indexs"] != "Team"))) {
                    // line 33
                    echo "            <th title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["league_standings_tooltip_array"]) ? $context["league_standings_tooltip_array"] : null), $context["indexs"], array(), "array"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_replace_filter($context["indexs"], array("_" => " "))), "html", null, true);
                    echo "</th>
          ";
                }
                // line 35
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indexs'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "        </thead>
          <tbody>
          ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), $context["data"], array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
                // line 39
                echo "            <tr>
              ";
                // line 40
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute($this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), $context["data"], array(), "array"), $context["index"], array(), "array")));
                foreach ($context['_seq'] as $context["_key"] => $context["indexs"]) {
                    // line 41
                    echo "              ";
                    if ((((($context["indexs"] != "Tm") && ($context["indexs"] != "Rk")) && ($context["indexs"] != "ID")) && ($context["indexs"] != "Team"))) {
                        // line 42
                        echo "              <td>";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), $context["data"], array(), "array"), $context["index"], array(), "array"), $context["indexs"], array(), "array"), "html", null, true);
                        echo "</td>
               ";
                    }
                    // line 44
                    echo "              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indexs'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "            </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "  
          </tbody>  
        </table> 
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "    </div>
    <br>
</div> 


";
    }

    public function getTemplateName()
    {
        return "specific_team_data.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 50,  140 => 46,  133 => 45,  127 => 44,  121 => 42,  118 => 41,  114 => 40,  111 => 39,  107 => 38,  103 => 36,  97 => 35,  89 => 33,  86 => 32,  82 => 31,  78 => 29,  74 => 28,  67 => 23,  60 => 18,  57 => 17,  48 => 10,  46 => 9,  42 => 7,  39 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends '_.twig' %}*/
/* */
/* {% block specific_team_data %}*/
/* <h1 class="specific_team_heading">{{heading}}</h1>*/
/* {% endblock %}*/
/* {% block main %}*/
/* <div class="selected_team_info" id="specific_team_data">*/
/*     <div class="sort_buttons_specific_team">*/
/*     {% if league_standings != 1 %}*/
/*       <button>PLAYER BASIC STATISTICS</button>*/
/*       <button>PLAYER ADVANCED STATISTICS</button>*/
/*       <button>TOI STATISTICS</button>*/
/*       <button>GOALIE STATISTICS</button>*/
/*       <button>TEAM STATISTICS</button>*/
/*       <button>TEAM ADVANCED STATISTICS</button>*/
/*      {% endif %}*/
/*   {% if league_standings == 1 %}*/
/*       <button>ATLANTIC DIVISION</button>*/
/*       <button>PACIFIC DIVISION</button>*/
/*       <button>METROPOLIAN DIVISION</button>*/
/*       <button>CENTRAL DIVISION</button>*/
/*   {% endif %}*/
/*     </div>*/
/*     <div id="maincontainer_big_table">*/
/* */
/*       <button class="table_reset">Reset sorting</button>*/
/*       <input type="search" class="light-table-filter fixed_header" data-table="order-table" placeholder="Search...">*/
/*       {% for data in specific_team_data|keys %}*/
/*         <table class="team_stats_list order-table sortable">*/
/*         <thead class="fixed_header">*/
/*         {% for indexs in specific_team_data[data][0]|keys %}*/
/*           {% if indexs != "Tm" and indexs != "Rk" and indexs != "ID"  and indexs != "Team" %}*/
/*             <th title="{{league_standings_tooltip_array[indexs]}}">{{indexs|replace({'_':' '})|upper}}</th>*/
/*           {% endif %}*/
/*         {% endfor %}*/
/*         </thead>*/
/*           <tbody>*/
/*           {% for index in specific_team_data[data]|keys %}*/
/*             <tr>*/
/*               {% for indexs in specific_team_data[data][index]|keys %}*/
/*               {% if indexs != "Tm" and indexs != "Rk" and indexs != "ID" and indexs != "Team" %}*/
/*               <td>{{specific_team_data[data][index][indexs]}}</td>*/
/*                {% endif %}*/
/*               {% endfor %}*/
/*             </tr>*/
/*           {% endfor %}  */
/*           </tbody>  */
/*         </table> */
/*       {% endfor %}*/
/*     </div>*/
/*     <br>*/
/* </div> */
/* */
/* */
/* {% endblock %}*/
