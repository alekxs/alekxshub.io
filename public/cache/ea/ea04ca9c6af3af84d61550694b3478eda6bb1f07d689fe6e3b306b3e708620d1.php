<?php

/* playoff_series_stats.twig */
class __TwigTemplate_4afba79a4e7b9b5d42d15cd0307ba750cb2aaeac194b5b2b06d5221b026e5041 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("_.twig", "playoff_series_stats.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "  <div class=\"data\" id=\"team_stats_reg\">
    <div class=\"team_stats_table\">
      <div class=\"scrollable_table\">


        <div id=\"maincontainer_small_table\">
          
  
      <input type=\"search\" class=\"light-table-filter fixed_header\" data-table=\"order-table\" placeholder=\"Search...\">
      <button class=\"table_reset\">Reset sorting</button>
        <table class=\"team_stats_list order-table sortable\">
          <thead class=\"fixed_header\">
            ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["series_stats"]) ? $context["series_stats"] : null), 0, array(), "array")));
        foreach ($context['_seq'] as $context["_key"] => $context["keys"]) {
            // line 17
            echo "            ";
            if (($context["keys"] != "Rk")) {
                // line 18
                echo "            <th>";
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, $context["keys"]), "html", null, true);
                echo "</th>
            ";
            }
            // line 20
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['keys'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "          </thead>
            <tbody>
              ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["series_stats"]) ? $context["series_stats"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
            // line 24
            echo "              <tr>
              ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["series_stats"]) ? $context["series_stats"] : null), $context["index"], array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
                // line 26
                echo "             ";
                if (($context["key"] != "Rk")) {
                    // line 27
                    echo "              <td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["series_stats"]) ? $context["series_stats"] : null), $context["index"], array(), "array"), $context["key"], array(), "array"), "html", null, true);
                    echo "</td>
             ";
                }
                // line 29
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "              </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "  
            </tbody>                         
        </table>
      
        </div>
      </div>
    </div>
  </div>


";
    }

    public function getTemplateName()
    {
        return "playoff_series_stats.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 31,  94 => 30,  88 => 29,  82 => 27,  79 => 26,  75 => 25,  72 => 24,  68 => 23,  64 => 21,  58 => 20,  52 => 18,  49 => 17,  45 => 16,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '_.twig' %}*/
/* */
/* {% block main %}*/
/*   <div class="data" id="team_stats_reg">*/
/*     <div class="team_stats_table">*/
/*       <div class="scrollable_table">*/
/* */
/* */
/*         <div id="maincontainer_small_table">*/
/*           */
/*   */
/*       <input type="search" class="light-table-filter fixed_header" data-table="order-table" placeholder="Search...">*/
/*       <button class="table_reset">Reset sorting</button>*/
/*         <table class="team_stats_list order-table sortable">*/
/*           <thead class="fixed_header">*/
/*             {% for keys in series_stats[0]|keys %}*/
/*             {% if  keys != "Rk" %}*/
/*             <th>{{keys|upper}}</th>*/
/*             {% endif %}*/
/*             {% endfor %}*/
/*           </thead>*/
/*             <tbody>*/
/*               {% for index in series_stats|keys %}*/
/*               <tr>*/
/*               {% for key in series_stats[index]|keys %}*/
/*              {% if  key != "Rk" %}*/
/*               <td>{{series_stats[index][key]}}</td>*/
/*              {% endif %}*/
/*               {% endfor %}*/
/*               </tr>*/
/*               {% endfor %}  */
/*             </tbody>                         */
/*         </table>*/
/*       */
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* */
/* */
/* {% endblock %}*/
