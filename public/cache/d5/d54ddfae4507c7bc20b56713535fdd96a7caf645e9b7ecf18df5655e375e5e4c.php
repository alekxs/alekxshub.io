<?php

/* team_stats.twig */
class __TwigTemplate_b495805e47aa7d92d7270cf5e37a85866abe1459958a157dd74365765c6ffcdf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("_.twig", "team_stats.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "\t<div class=\"data\" id=\"team_stats_reg\">
\t\t<div class=\"team_stats_table\">
\t\t\t<h1>";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["heading"]) ? $context["heading"] : null), "html", null, true);
        echo "</h1>
\t\t\t\t<table class=\"team_stats_list\">
\t\t\t\t\t";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["team_stats"]) ? $context["team_stats"] : null), 0, array(), "array")));
        foreach ($context['_seq'] as $context["_key"] => $context["keys"]) {
            // line 9
            echo "\t\t\t\t\t";
            if (($context["keys"] != "ID")) {
                // line 10
                echo "\t\t\t\t\t<th>";
                echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
                echo "</th>
\t\t\t\t\t";
            }
            // line 12
            echo "\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['keys'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["team_stats"]) ? $context["team_stats"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
            // line 15
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["team_stats"]) ? $context["team_stats"] : null), $context["index"], array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
                // line 17
                echo "\t\t\t\t\t\t";
                if (($context["key"] != "ID")) {
                    // line 18
                    echo "\t\t\t\t\t\t<td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["team_stats"]) ? $context["team_stats"] : null), $context["index"], array(), "array"), $context["key"], array(), "array"), "html", null, true);
                    echo "</td>
\t\t\t\t\t\t";
                }
                // line 20
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "  
\t\t\t\t\t</tbody>                         
\t\t\t\t</table>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "team_stats.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 22,  88 => 21,  82 => 20,  76 => 18,  73 => 17,  69 => 16,  66 => 15,  62 => 14,  59 => 13,  53 => 12,  47 => 10,  44 => 9,  40 => 8,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '_.twig' %}*/
/* */
/* {% block main %}*/
/* 	<div class="data" id="team_stats_reg">*/
/* 		<div class="team_stats_table">*/
/* 			<h1>{{heading}}</h1>*/
/* 				<table class="team_stats_list">*/
/* 					{% for keys in team_stats[0]|keys %}*/
/* 					{% if keys != "ID" %}*/
/* 					<th>{{keys}}</th>*/
/* 					{% endif %}*/
/* 					{% endfor %}*/
/* 					<tbody>*/
/* 						{% for index in team_stats|keys %}*/
/* 						<tr>*/
/* 						{% for key in team_stats[index]|keys %}*/
/* 						{% if key != "ID" %}*/
/* 						<td>{{team_stats[index][key]}}</td>*/
/* 						{% endif %}*/
/* 						{% endfor %}*/
/* 						</tr>*/
/* 						{% endfor %}  */
/* 					</tbody>                         */
/* 				</table>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock main %}*/
/* */
