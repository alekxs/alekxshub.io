<?php

/* admin.twig */
class __TwigTemplate_2a5d61f0912723d93758722945d728ccebe45ee87e50a236f1fa52971b0560ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("_.twig", "admin.twig", 1);
        $this->blocks = array(
            'specific_team_data' => array($this, 'block_specific_team_data'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_specific_team_data($context, array $blocks = array())
    {
        echo "  
<p>Hello, Admin!</p>
<button onclick=\"window.location.href='http://logout:logout@nhl.dev'\" class=\"admin_button\">Logout</button>
";
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "\t
<table class=\"admin_table\">
<thead>
\t<th>Table name</th>
\t<th>last updated at</th>
\t<th>update</th>
</thead>
\t\t<tbody>

\t\t\t ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["table_names"]) ? $context["table_names"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
            // line 18
            echo "\t\t\t\t<tr>
\t\t\t\t<td>";
            // line 19
            echo twig_escape_filter($this->env, $context["index"], "html", null, true);
            echo "</td>
\t\t\t\t<td>";
            // line 20
            echo twig_escape_filter($this->env, twig_replace_filter($this->getAttribute((isset($context["last_updated_at"]) ? $context["last_updated_at"] : null), $this->getAttribute($context["loop"], "index0", array()), array(), "array"), array("[" => "", "]" => "")), "html", null, true);
            echo "</td>
\t\t\t\t<td><a href=\"/admin/";
            // line 21
            echo twig_escape_filter($this->env, $context["index"], "html", null, true);
            echo "\">update</a><td>
\t\t\t\t</tr>
\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "\t
\t\t</tbody> 
</table>
";
    }

    public function getTemplateName()
    {
        return "admin.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 24,  80 => 21,  76 => 20,  72 => 19,  69 => 18,  52 => 17,  41 => 8,  38 => 7,  29 => 2,  11 => 1,);
    }
}
/* {% extends '_.twig' %}*/
/* {% block specific_team_data %}  */
/* <p>Hello, Admin!</p>*/
/* <button onclick="window.location.href='http://logout:logout@nhl.dev'" class="admin_button">Logout</button>*/
/* {% endblock  specific_team_data %}   */
/* */
/* {% block main %}*/
/* 	*/
/* <table class="admin_table">*/
/* <thead>*/
/* 	<th>Table name</th>*/
/* 	<th>last updated at</th>*/
/* 	<th>update</th>*/
/* </thead>*/
/* 		<tbody>*/
/* */
/* 			 {% for index in table_names %}*/
/* 				<tr>*/
/* 				<td>{{index}}</td>*/
/* 				<td>{{last_updated_at[loop.index0]|replace({'[':'', ']': ''})}}</td>*/
/* 				<td><a href="/admin/{{index}}">update</a><td>*/
/* 				</tr>*/
/* 			{% endfor %}*/
/* 	*/
/* 		</tbody> */
/* </table>*/
/* {% endblock  main %}  */
