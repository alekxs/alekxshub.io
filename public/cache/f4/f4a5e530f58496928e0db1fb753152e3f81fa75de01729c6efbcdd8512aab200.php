<?php

/* league_standings.twig */
class __TwigTemplate_a9f3445f3d76d94d794b4af44d467cdc2715260698133a2e140457540fd17f4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("_.twig", "league_standings.twig", 1);
        $this->blocks = array(
            'specific_team_data' => array($this, 'block_specific_team_data'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_specific_team_data($context, array $blocks = array())
    {
        // line 4
        echo "

<div class=\"selected_team_info\">
  <div class=\"sort_buttons_division_standing\">
  
    <button value=\"ad\">ATLANTIC DIVISION</button>
    <button value=\"md\">METROPOLIAN DIVISION</button>
    <button value=\"pd\">PACIFIC DIVISION</button>
    <button value=\"cd\">CENTRAL DIVISION</button>
  </div>
    <div id=\"maincontainer_big_table\">
      <div id=\"childcontainer_big_table\">

      <input type=\"search\" class=\"light-table-filter\" data-table=\"order-table\" placeholder=\"Search...\">

        <table class=\"team_stats_list order-table\"  id=\"division_table\">
        <thead>
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["league_standings_array"]) ? $context["league_standings_array"] : null), 0, array(), "array")));
        foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
            // line 22
            echo "         
            <th>";
            // line 23
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_replace_filter($context["index"], array("_" => " "))), "html", null, true);
            echo "</th>
        
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "        </thead>
          <tbody>
          ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["league_standings_array"]) ? $context["league_standings_array"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["indexs"]) {
            // line 29
            echo "            <tr>
              
                ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["league_standings_array"]) ? $context["league_standings_array"] : null), $context["indexs"], array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["inde"]) {
                // line 32
                echo "              <td>";
                echo twig_escape_filter($this->env, $context["inde"], "html", null, true);
                echo "</td>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['inde'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "  
            </tr>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indexs'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "  
          </tbody>  
        </table> 

      </div>
    </div>
    <br>
</div> 


";
    }

    public function getTemplateName()
    {
        return "league_standings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 35,  91 => 33,  82 => 32,  78 => 31,  74 => 29,  70 => 28,  66 => 26,  57 => 23,  54 => 22,  50 => 21,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '_.twig' %}*/
/* */
/* {% block specific_team_data %}*/
/* */
/* */
/* <div class="selected_team_info">*/
/*   <div class="sort_buttons_division_standing">*/
/*   */
/*     <button value="ad">ATLANTIC DIVISION</button>*/
/*     <button value="md">METROPOLIAN DIVISION</button>*/
/*     <button value="pd">PACIFIC DIVISION</button>*/
/*     <button value="cd">CENTRAL DIVISION</button>*/
/*   </div>*/
/*     <div id="maincontainer_big_table">*/
/*       <div id="childcontainer_big_table">*/
/* */
/*       <input type="search" class="light-table-filter" data-table="order-table" placeholder="Search...">*/
/* */
/*         <table class="team_stats_list order-table"  id="division_table">*/
/*         <thead>*/
/*         {% for index in league_standings_array[0]|keys %}*/
/*          */
/*             <th>{{index|replace({'_':' '})|upper}}</th>*/
/*         */
/*         {% endfor %}*/
/*         </thead>*/
/*           <tbody>*/
/*           {% for indexs in league_standings_array|keys %}*/
/*             <tr>*/
/*               */
/*                 {% for inde in league_standings_array[indexs]%}*/
/*               <td>{{inde}}</td>*/
/*                 {% endfor %}  */
/*             </tr>*/
/*           {% endfor %}  */
/*           </tbody>  */
/*         </table> */
/* */
/*       </div>*/
/*     </div>*/
/*     <br>*/
/* </div> */
/* */
/* */
/* {% endblock %}*/
