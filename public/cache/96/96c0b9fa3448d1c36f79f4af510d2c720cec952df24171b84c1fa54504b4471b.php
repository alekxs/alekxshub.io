<?php

/* _.twig */
class __TwigTemplate_7f9b64590bb79dd0ad9800280f978e2ba25473cf9940d1fa01009cb572362211 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'specific_team_data' => array($this, 'block_specific_team_data'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <title>Kvalifikācijas darbs</title>
        <meta charset=\"utf-8\"/>
        <link href='//fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
        <link rel=\"stylesheet\" type=\"text/less\" href=\"../css/front.less\">
        <link rel=\"stylesheet\" type=\"text/less\" href=\"../css/global/global.less\">
        <link href=\"../css/fontello.css\" rel=\"stylesheet\" type=\"text/css\">
        <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\" type=\"text/javascript\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/less.js/2.5.3/less.min.js\" type=\"text/javascript\"></script>
        <script src=\"../js/mixitup.js\" type=\"text/javascript\"></script>
        <script src=\"../js/functions.js\" type=\"text/javascript\"></script>
        <script src=\"../js/sorttable.js\" type=\"text/javascript\"></script>
        
    </head>
    <body> 
        <div id=\"container\">
          <div class=\"content\">
 
            <div id=\"header\">
            
              <div id=\"images\">
                <a href=\"/team/ANA\"><img src=\"../img/ducks.png\" class=\"header_logo_top\" attr=\"ANAHEIM DUCKS\"></a>
                <a href=\"/team/ARI\"><img src=\"../img/arizona.png\" class=\"header_logo_top\" attr=\"ARIZONA COYOTES\"></a>
                <a href=\"/team/BOS\"><img src=\"../img/boston.png\" class=\"header_logo_top\" attr=\"BOSTON BRUINS\"></a>
                <a href=\"/team/BUF\"><img src=\"../img/buffalo.png\" class=\"header_logo_top\" attr=\"BUFFALO SABRES\"></a>
                <a href=\"/team/CGY\"><img src=\"../img/flames.png\" class=\"header_logo_top\" attr=\"CALGARY FLAMES\"></a>
                <a href=\"/team/CAR\"><img src=\"../img/carolina.png\" class=\"header_logo_top\" attr=\"CAROLINA HURRICANES\"></a>
                <a href=\"/team/CHI\"><img src=\"../img/hawks.png\" class=\"header_logo_top\" attr=\"CHICAGO BLACKHAWKS\"></a>
                <a href=\"/team/COL\"><img src=\"../img/avalanche.png\" class=\"header_logo_top\" attr=\"COLORADO AVALANCHE\">
                <a href=\"/team/CBJ\"><img src=\"../img/columbus.png\" class=\"header_logo_top\" attr=\"COLUMBUS BLUE JACKETS\"></a>
                <a href=\"/team/DAL\"><img src=\"../img/dalas.png\" class=\"header_logo_top\" attr=\"DALLAS STARS\"></a>
                <a href=\"/team/DET\"><img src=\"../img/detroit.png\" class=\"header_logo_top\" attr=\"DETROIT RED WINGS\"></a>
                <a href=\"/team/EDM\"><img src=\"../img/oilers.png\" class=\"header_logo_top\" attr=\"EDMONTON OILERS\"></a>
                <a href=\"/team/FLA\"><img src=\"../img/panters.png\" class=\"header_logo_top\" attr=\"FLORIDA PANTHERS\"></a>
                <a href=\"/team/LAK\"><img src=\"../img/kings.png\" class=\"header_logo_top\" attr=\"LOS ANGELES KINGS\"></a>
                <a href=\"/team/MIN\"><img src=\"../img/wild.png\" class=\"header_logo_top\" attr=\"MINNESOTA WILD\"></a>
                <a href=\"/team/MTL\"><img src=\"../img/montreal.png\" class=\"header_logo_top\" attr=\"MONTREAL CANADIENS\"></a>
                <a href=\"/team/NSH\"><img src=\"../img/predators.png\" class=\"header_logo_top\" attr=\"NASHVILLE PREDATORS\"></a>
                <a href=\"/team/NJD\"><img src=\"../img/devils.png\" class=\"header_logo_top\" attr=\"NEW JERSEY DEVILS\"></a>
                <a href=\"/team/NYI\"><img src=\"../img/islanders.png\" class=\"header_logo_top\" attr=\"NEW YORK ISLANDERS\"></a>
                <a href=\"/team/NYR\"><img src=\"../img/rangers.png\" class=\"header_logo_top\" attr=\"NEW YORK RANGERS\"></a>
                <a href=\"/team/OTT\"><img src=\"../img/senators.png\" class=\"header_logo_top\" attr=\"OTTAWA SENATORS\"></a>
                <a href=\"/team/PHI\"><img src=\"../img/flyers.png\" class=\"header_logo_top\" attr=\"PHILADELPHIA FLYERS\"></a>
                <a href=\"/team/PIT\"><img src=\"../img/penguins.png\" class=\"header_logo_top\" attr=\"PITTSBURGH PENGUINS\"></a>
                <a href=\"/team/SJS\"><img src=\"../img/sharks.png\" class=\"header_logo_top\" attr=\"SAN JOSE SHARKS\"></a>
                <a href=\"/team/STL\"><img src=\"../img/blues.png\" class=\"header_logo_top\" attr=\"ST. LOUIS BLUES\"></a>
                <a href=\"/team/TBL\"><img src=\"../img/tampa.png\" class=\"header_logo_top\" attr=\"TAMPA BAY LIGHTNING\"></a>
                <a href=\"/team/TOR\"><img src=\"../img/toronto.png\" class=\"header_logo_top\" attr=\"TORONTO MAPLE LEAFS\"></a>
                <a href=\"/team/VAN\"><img src=\"../img/canucks.png\" class=\"header_logo_top\" attr=\"VANCOUVER CANUCKS\"></a>
                <a href=\"/team/WSH\"><img src=\"../img/capitals.png\" class=\"header_logo_top\" attr=\"WASHINGTON CAPITALS\"></a>
                <a href=\"/team/WPG\"><img src=\"../img/jets.png\" class=\"header_logo_top\" attr=\"WINNIPEG JETS\"></a>
              </div>

                  <div id=\"nav-bar\">
                    <div class=\"right\">
                      <div id=\"logo\"></div>
                    </div>
                      <div id='cssmenu'>
                      <ul>
                         <li><a href='/'><span>HOME</span></a></li>
                         <li class='has-sub'><a href='#'><span>REGULAR SEASON</span></a>
                            <ul>
                               <li><a href='/league_standings'><span>LEAGUE STANDINGS</span></a>
                               </li>
                               <li><a href='/league_leaders'><span>LEAGUE LEADERS</span></a>
                               </li>
                               <li class='has-sub'><a href='#'><span>TEAM STATISTICS</span></a>
                                  <ul>
                                     <li><a href='/basic_stats'><span>BASIC TEAM STATISTICS</span></a></li>
                                     <li class='last'><a href='/advanced_stats'><span>ADVANCED TEAM STATISTICS</span></a></li>
                                  </ul>
                               </li>
                               <li class='has-sub'><a href='#'><span>PLAYER STATISTICS</span></a>
                                  <ul>
                                     <li><a href='/skaters_basic_stats'><span>SKATER BASIC STATISTICS</span></a></li>
                                     <li><a href='/skaters_advanced_stats'><span>SKATER ADVANCED STATISTICS</span></a></li>
                                     <li><a href='/skaters_toi_stats'><span>SKATER TOI STATISTICS</span></a></li>
                                     <li><a href='/goalies_stats'><span>GOALIE STATISTICS</span></a></li>
                                  </ul>
                               </li>
                            </ul>
                         </li>
                         <li class='has-sub'><a href=''><span>PLAYOFF</span></a>
                            <ul>
                             <li><a href='/league_leaders_playoff'><span>LEAGUE LEADERS</span></a>
                             <li><a href='/playoff_series_stats'><span>TEAM STATISTICS</span></a>
                             <li class='has-sub'><a href='#'><span>PLAYER STATISTICS</span></a>
                                <ul>
                                   <li><a href='/skaters_basic_stats_playoffs'><span>SKATER BASIC STATISTICS</span></a></li>
                                   <li><a href='/skaters_advanced_stats_playoffs'><span>SKATER ADVANCED STATISTICS</span></a></li>
                                   <li><a href='/skaters_toi_stats_playoffs'><span>SKATER TOI STATISTICS</span></a></li>
                                   <li><a href='/goalies_stats_playoffs'><span>GOALIE STATISTICS</span></a></li>
                                </ul>
                             </li>
                             
                          </ul></li>
                         <li><a href='' class=\"icon-calendar-1 calendar_icon\" onMouseOver=\"show_calendar();\" onMouseOut=\"hide_calendar();\"><span></span></a></li>
                      </ul>
                      </div>
                  </div>
                 <h1 class=\"selected_header\"></h1>
";
        // line 103
        $this->displayBlock('specific_team_data', $context, $blocks);
        // line 105
        echo "        

              </div>
                <div class=\"calendar\">
                  <p class=\"calendar_date\">";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["calendar_data"]) ? $context["calendar_data"] : null), 0, array(), "array"), 0, array(), "array"), "html", null, true);
        echo "</p>
                  <img src=\"../img/playoff_logo.png\" class=\"calendar_logo\">
                  <div class=\"calendar_content\">
                    <div class=\"home\">
                      <p class=\"game_status st_home\">HOME</p>
                        ";
        // line 114
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["calendar_data"]) ? $context["calendar_data"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
            // line 115
            echo "                          <p>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["calendar_data"]) ? $context["calendar_data"] : null), $context["index"], array(), "array"), 2, array(), "array"), "html", null, true);
            echo "<br>";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["calendar_prognosis_data"]) ? $context["calendar_prognosis_data"] : null), $context["index"], array(), "array"), 0, array(), "array"), 2), "html", null, true);
            echo "%</p>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "                    </div>
                      <div class=\"away st_away\">
                        <p class=\"game_status st_away\">AWAY</p>
                          ";
        // line 120
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["calendar_data"]) ? $context["calendar_data"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
            // line 121
            echo "                            <p>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["calendar_data"]) ? $context["calendar_data"] : null), $context["index"], array(), "array"), 1, array(), "array"), "html", null, true);
            echo "<br>";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["calendar_prognosis_data"]) ? $context["calendar_prognosis_data"] : null), $context["index"], array(), "array"), 1, array(), "array"), 2), "html", null, true);
            echo "%</p>
                          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "                      </div>
                    </div>
                </div>
                  <div class=\"sort_buttons\">
                    <button class=\"filter\" data-filter=\".category-m\">METROPOLIAN</button>
                    <button class=\"filter\" data-filter=\".category-p\">PACIFIC</button>
                    <button class=\"filter\" data-filter=\".category-a\">ATLANTIC</button>
                    <button class=\"filter\" data-filter=\".category-c\">CENTRAL</button>
                    <button class=\"filter\" data-filter=\".category-all\">ALL</button>
                  </div>
                      <div id=\"Container\" class=\"container\">
                        <div class=\"container_main\">
                          <div class=\"columns\">
                            ";
        // line 136
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["franchise_data"]) ? $context["franchise_data"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
            // line 137
            echo "                              <div class=\"ticket-frame mix category-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conference_id"]) ? $context["conference_id"] : null), $context["index"], array(), "array"), "html", null, true);
            echo " category-all\">
                               <a href=\"/team/";
            // line 138
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reference_to_info"]) ? $context["reference_to_info"] : null), $context["index"], array(), "array"), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["images_main"]) ? $context["images_main"] : null), $context["index"], array(), "array"), "html", null, true);
            echo "\" class=\"white-circle\"></a>
                                <h4>";
            // line 139
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["franchise_data"]) ? $context["franchise_data"] : null), $context["index"], array(), "array"), "Franchise", array(), "array")), "html", null, true);
            echo "</h4>
                                <div class=\"frame-paragraph\">
                                  ";
            // line 141
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["franchise_data"]) ? $context["franchise_data"] : null), $context["index"], array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["values"]) {
                // line 142
                echo "                                    ";
                if ((($context["values"] != "Franchise") && ($context["values"] != "Lg"))) {
                    // line 143
                    echo "                                      <p>";
                    echo twig_escape_filter($this->env, twig_replace_filter($context["values"], array("From" => "First year of NHL career", "To" => "Last year of NHL career", "Yrs" => "Total years in NHL", "GP" => "Games played", "W" => "Wins", "L" => "Losses", "T" => "Ties", "OL" => "Overtime/shootout Losses", "PTS" => "Points", "PTS%" => "Points percentage", "Yrs Plyf" => "Years team made the playoffs", "Div" => "Finished first in the division", "Conf" => "Won the playoff conference championship", "Champ" => "Won the league championship", "St Cup" => "Won the Stanley Cup")), "html", null, true);
                    echo " : ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["franchise_data"]) ? $context["franchise_data"] : null), $context["index"], array(), "array"), $context["values"], array(), "array"), "html", null, true);
                    echo "</p>
                                      <br>
                                    ";
                }
                // line 146
                echo "                                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['values'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 147
            echo "                                </div>
                              </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 150
        echo "                          </div>
                        </div>
                      </div>
";
        // line 153
        $this->displayBlock('main', $context, $blocks);
        // line 156
        echo "  </div>
</div>

    </body>
</html>
";
    }

    // line 103
    public function block_specific_team_data($context, array $blocks = array())
    {
        echo "  

";
    }

    // line 153
    public function block_main($context, array $blocks = array())
    {
        // line 154
        echo "
";
    }

    public function getTemplateName()
    {
        return "_.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 154,  265 => 153,  257 => 103,  248 => 156,  246 => 153,  241 => 150,  233 => 147,  227 => 146,  218 => 143,  215 => 142,  211 => 141,  206 => 139,  200 => 138,  195 => 137,  191 => 136,  176 => 123,  165 => 121,  161 => 120,  156 => 117,  145 => 115,  141 => 114,  133 => 109,  127 => 105,  125 => 103,  21 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         <title>Kvalifikācijas darbs</title>*/
/*         <meta charset="utf-8"/>*/
/*         <link href='//fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>*/
/*         <link rel="stylesheet" type="text/less" href="../css/front.less">*/
/*         <link rel="stylesheet" type="text/less" href="../css/global/global.less">*/
/*         <link href="../css/fontello.css" rel="stylesheet" type="text/css">*/
/*         <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>*/
/*         <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.5.3/less.min.js" type="text/javascript"></script>*/
/*         <script src="../js/mixitup.js" type="text/javascript"></script>*/
/*         <script src="../js/functions.js" type="text/javascript"></script>*/
/*         <script src="../js/sorttable.js" type="text/javascript"></script>*/
/*         */
/*     </head>*/
/*     <body> */
/*         <div id="container">*/
/*           <div class="content">*/
/*  */
/*             <div id="header">*/
/*             */
/*               <div id="images">*/
/*                 <a href="/team/ANA"><img src="../img/ducks.png" class="header_logo_top" attr="ANAHEIM DUCKS"></a>*/
/*                 <a href="/team/ARI"><img src="../img/arizona.png" class="header_logo_top" attr="ARIZONA COYOTES"></a>*/
/*                 <a href="/team/BOS"><img src="../img/boston.png" class="header_logo_top" attr="BOSTON BRUINS"></a>*/
/*                 <a href="/team/BUF"><img src="../img/buffalo.png" class="header_logo_top" attr="BUFFALO SABRES"></a>*/
/*                 <a href="/team/CGY"><img src="../img/flames.png" class="header_logo_top" attr="CALGARY FLAMES"></a>*/
/*                 <a href="/team/CAR"><img src="../img/carolina.png" class="header_logo_top" attr="CAROLINA HURRICANES"></a>*/
/*                 <a href="/team/CHI"><img src="../img/hawks.png" class="header_logo_top" attr="CHICAGO BLACKHAWKS"></a>*/
/*                 <a href="/team/COL"><img src="../img/avalanche.png" class="header_logo_top" attr="COLORADO AVALANCHE">*/
/*                 <a href="/team/CBJ"><img src="../img/columbus.png" class="header_logo_top" attr="COLUMBUS BLUE JACKETS"></a>*/
/*                 <a href="/team/DAL"><img src="../img/dalas.png" class="header_logo_top" attr="DALLAS STARS"></a>*/
/*                 <a href="/team/DET"><img src="../img/detroit.png" class="header_logo_top" attr="DETROIT RED WINGS"></a>*/
/*                 <a href="/team/EDM"><img src="../img/oilers.png" class="header_logo_top" attr="EDMONTON OILERS"></a>*/
/*                 <a href="/team/FLA"><img src="../img/panters.png" class="header_logo_top" attr="FLORIDA PANTHERS"></a>*/
/*                 <a href="/team/LAK"><img src="../img/kings.png" class="header_logo_top" attr="LOS ANGELES KINGS"></a>*/
/*                 <a href="/team/MIN"><img src="../img/wild.png" class="header_logo_top" attr="MINNESOTA WILD"></a>*/
/*                 <a href="/team/MTL"><img src="../img/montreal.png" class="header_logo_top" attr="MONTREAL CANADIENS"></a>*/
/*                 <a href="/team/NSH"><img src="../img/predators.png" class="header_logo_top" attr="NASHVILLE PREDATORS"></a>*/
/*                 <a href="/team/NJD"><img src="../img/devils.png" class="header_logo_top" attr="NEW JERSEY DEVILS"></a>*/
/*                 <a href="/team/NYI"><img src="../img/islanders.png" class="header_logo_top" attr="NEW YORK ISLANDERS"></a>*/
/*                 <a href="/team/NYR"><img src="../img/rangers.png" class="header_logo_top" attr="NEW YORK RANGERS"></a>*/
/*                 <a href="/team/OTT"><img src="../img/senators.png" class="header_logo_top" attr="OTTAWA SENATORS"></a>*/
/*                 <a href="/team/PHI"><img src="../img/flyers.png" class="header_logo_top" attr="PHILADELPHIA FLYERS"></a>*/
/*                 <a href="/team/PIT"><img src="../img/penguins.png" class="header_logo_top" attr="PITTSBURGH PENGUINS"></a>*/
/*                 <a href="/team/SJS"><img src="../img/sharks.png" class="header_logo_top" attr="SAN JOSE SHARKS"></a>*/
/*                 <a href="/team/STL"><img src="../img/blues.png" class="header_logo_top" attr="ST. LOUIS BLUES"></a>*/
/*                 <a href="/team/TBL"><img src="../img/tampa.png" class="header_logo_top" attr="TAMPA BAY LIGHTNING"></a>*/
/*                 <a href="/team/TOR"><img src="../img/toronto.png" class="header_logo_top" attr="TORONTO MAPLE LEAFS"></a>*/
/*                 <a href="/team/VAN"><img src="../img/canucks.png" class="header_logo_top" attr="VANCOUVER CANUCKS"></a>*/
/*                 <a href="/team/WSH"><img src="../img/capitals.png" class="header_logo_top" attr="WASHINGTON CAPITALS"></a>*/
/*                 <a href="/team/WPG"><img src="../img/jets.png" class="header_logo_top" attr="WINNIPEG JETS"></a>*/
/*               </div>*/
/* */
/*                   <div id="nav-bar">*/
/*                     <div class="right">*/
/*                       <div id="logo"></div>*/
/*                     </div>*/
/*                       <div id='cssmenu'>*/
/*                       <ul>*/
/*                          <li><a href='/'><span>HOME</span></a></li>*/
/*                          <li class='has-sub'><a href='#'><span>REGULAR SEASON</span></a>*/
/*                             <ul>*/
/*                                <li><a href='/league_standings'><span>LEAGUE STANDINGS</span></a>*/
/*                                </li>*/
/*                                <li><a href='/league_leaders'><span>LEAGUE LEADERS</span></a>*/
/*                                </li>*/
/*                                <li class='has-sub'><a href='#'><span>TEAM STATISTICS</span></a>*/
/*                                   <ul>*/
/*                                      <li><a href='/basic_stats'><span>BASIC TEAM STATISTICS</span></a></li>*/
/*                                      <li class='last'><a href='/advanced_stats'><span>ADVANCED TEAM STATISTICS</span></a></li>*/
/*                                   </ul>*/
/*                                </li>*/
/*                                <li class='has-sub'><a href='#'><span>PLAYER STATISTICS</span></a>*/
/*                                   <ul>*/
/*                                      <li><a href='/skaters_basic_stats'><span>SKATER BASIC STATISTICS</span></a></li>*/
/*                                      <li><a href='/skaters_advanced_stats'><span>SKATER ADVANCED STATISTICS</span></a></li>*/
/*                                      <li><a href='/skaters_toi_stats'><span>SKATER TOI STATISTICS</span></a></li>*/
/*                                      <li><a href='/goalies_stats'><span>GOALIE STATISTICS</span></a></li>*/
/*                                   </ul>*/
/*                                </li>*/
/*                             </ul>*/
/*                          </li>*/
/*                          <li class='has-sub'><a href=''><span>PLAYOFF</span></a>*/
/*                             <ul>*/
/*                              <li><a href='/league_leaders_playoff'><span>LEAGUE LEADERS</span></a>*/
/*                              <li><a href='/playoff_series_stats'><span>TEAM STATISTICS</span></a>*/
/*                              <li class='has-sub'><a href='#'><span>PLAYER STATISTICS</span></a>*/
/*                                 <ul>*/
/*                                    <li><a href='/skaters_basic_stats_playoffs'><span>SKATER BASIC STATISTICS</span></a></li>*/
/*                                    <li><a href='/skaters_advanced_stats_playoffs'><span>SKATER ADVANCED STATISTICS</span></a></li>*/
/*                                    <li><a href='/skaters_toi_stats_playoffs'><span>SKATER TOI STATISTICS</span></a></li>*/
/*                                    <li><a href='/goalies_stats_playoffs'><span>GOALIE STATISTICS</span></a></li>*/
/*                                 </ul>*/
/*                              </li>*/
/*                              */
/*                           </ul></li>*/
/*                          <li><a href='' class="icon-calendar-1 calendar_icon" onMouseOver="show_calendar();" onMouseOut="hide_calendar();"><span></span></a></li>*/
/*                       </ul>*/
/*                       </div>*/
/*                   </div>*/
/*                  <h1 class="selected_header"></h1>*/
/* {% block specific_team_data %}  */
/* */
/* {% endblock  specific_team_data %}        */
/* */
/*               </div>*/
/*                 <div class="calendar">*/
/*                   <p class="calendar_date">{{calendar_data[0][0]}}</p>*/
/*                   <img src="../img/playoff_logo.png" class="calendar_logo">*/
/*                   <div class="calendar_content">*/
/*                     <div class="home">*/
/*                       <p class="game_status st_home">HOME</p>*/
/*                         {% for index in calendar_data|keys %}*/
/*                           <p>{{calendar_data[index][2]}}<br>{{calendar_prognosis_data[index][0]|number_format(2)}}%</p>*/
/*                         {% endfor %}*/
/*                     </div>*/
/*                       <div class="away st_away">*/
/*                         <p class="game_status st_away">AWAY</p>*/
/*                           {% for index in calendar_data|keys %}*/
/*                             <p>{{calendar_data[index][1]}}<br>{{calendar_prognosis_data[index][1]|number_format(2)}}%</p>*/
/*                           {% endfor %}*/
/*                       </div>*/
/*                     </div>*/
/*                 </div>*/
/*                   <div class="sort_buttons">*/
/*                     <button class="filter" data-filter=".category-m">METROPOLIAN</button>*/
/*                     <button class="filter" data-filter=".category-p">PACIFIC</button>*/
/*                     <button class="filter" data-filter=".category-a">ATLANTIC</button>*/
/*                     <button class="filter" data-filter=".category-c">CENTRAL</button>*/
/*                     <button class="filter" data-filter=".category-all">ALL</button>*/
/*                   </div>*/
/*                       <div id="Container" class="container">*/
/*                         <div class="container_main">*/
/*                           <div class="columns">*/
/*                             {% for index in franchise_data|keys %}*/
/*                               <div class="ticket-frame mix category-{{conference_id[index]}} category-all">*/
/*                                <a href="/team/{{reference_to_info[index]}}"><img src="{{images_main[index]}}" class="white-circle"></a>*/
/*                                 <h4>{{franchise_data[index]['Franchise']|upper}}</h4>*/
/*                                 <div class="frame-paragraph">*/
/*                                   {% for values in franchise_data[index]|keys %}*/
/*                                     {% if values != "Franchise" and values != "Lg" %}*/
/*                                       <p>{{values|replace({'From':'First year of NHL career', 'To':'Last year of NHL career', 'Yrs':'Total years in NHL', 'GP':'Games played', 'W':'Wins', 'L':'Losses', 'T':'Ties', 'OL':'Overtime/shootout Losses', 'PTS':'Points', 'PTS%':'Points percentage', 'Yrs Plyf':'Years team made the playoffs', 'Div':'Finished first in the division', 'Conf':'Won the playoff conference championship', 'Champ':'Won the league championship', 'St Cup':'Won the Stanley Cup'})}} : {{franchise_data[index][values]}}</p>*/
/*                                       <br>*/
/*                                     {% endif %}*/
/*                                   {% endfor %}*/
/*                                 </div>*/
/*                               </div>*/
/*                             {% endfor %}*/
/*                           </div>*/
/*                         </div>*/
/*                       </div>*/
/* {% block main %}*/
/* */
/* {% endblock main %}*/
/*   </div>*/
/* </div>*/
/* */
/*     </body>*/
/* </html>*/
/* {#*/
/* */
/*                               <ul class="nav_bar">*/
/*                                   <li><a href="">PLAY OFF</a></li>*/
/*                                   <li><a href="/regular">REGULAR SEASON</a></li>*/
/*                                   <li><a href="" >ABOUT</a></li>*/
/*                                   <li><a href="/">HOME</a></li>*/
/*                                 </ul>*/
/* */
/* */
/* */
/*                                 css*/
/* */
/* .nav_bar ,ul{ list-style-type: none;margin: 0;padding: 0;overflow: hidden; background-color: #595959; height: 50px; margin-right: 50px;}*/
/* .nav_bar li { float: right; }*/
/* .nav_bar li a { display: block;color: white;text-align: center;padding: 16px 16px;text-decoration: none; font-size: 20px; }*/
/*                             <div class="left" id="main_menu">*/
/*                               <ul id="nav">*/
/*                                 <li><a href="/">HOME</a></li>*/
/*                                 <li><a href="">ABOUT</a></li>*/
/*                                 <li><a href="">REGULAR SEASON</a>*/
/*                                   <ul>*/
/*                                   <li><a href="">LEAGUE STANDINGS</a>*/
/*                                     <ul>*/
/*                                       <li><a href="/atlantic_division">ATLANTIC DIVISION</a></li>*/
/*                                       <li><a href="/metropolian_division">METROPOLIAN DIVISION</a></li>*/
/*                                       <li><a href="/central_division">CENTRAL DIVISION</a></li>*/
/*                                       <li><a href="/pacific_division">PACIFIC DIVISION</a></li>*/
/*                                     </ul>*/
/*                                   </li>*/
/*                                   <li><a href="/league_leaders">LEAGUE LEADERS</a>*/
/*                                   <li><a href="">TEAM STATISTICS</a>*/
/*                                     <ul>*/
/*                                       <li><a href="/team_stats">TEAM STATISTICS</a></li>*/
/*                                       <li><a href="/advanced_stats">ADVANCED TEAM STATISTICS</a></li>*/
/*                                     </ul>*/
/*                                   </li>*/
/*                                   <li><a href="">PLAYER STATISTICS</a>*/
/*                                     <ul>*/
/*                                       <li><a href="/skaters_basic_stats">SKATER BASIC STATISTICS</a></li>*/
/*                                       <li><a href="/skaters_advanced_stats">SKATER ADVANCED STATISTICS</a></li>*/
/*                                       <li><a href="/skaters_toi_stats">SKATER TOI STATISTICS</a></li>*/
/*                                       <li><a href="/goalies_stats">GOALIE STATISTICS</a></li>*/
/*                                     </ul>*/
/*                                   </li>*/
/*                                   </ul>*/
/*                                 </li>*/
/*                                 <li><a href="">PLAY OFF</a></li>*/
/*                               </ul>*/
/*                             </div>*/
/* */
/* */
/* */
/*                                             <div class="team-selection">*/
/*                   <p>Choose your favourite team: </p>*/
/*                     <select id="fav_team_drop" class="favourite_menu_dropdown">*/
/*                       <option value="none">Hide favourite team</option>*/
/*                       <option value="Anaheim">Anaheim Ducks</option>*/
/*                       <option value="Arizona">Arizona Coyotes</option>*/
/*                       <option value="Boston">Boston Bruins</option>*/
/*                       <option value="Buffalo">Buffalo Sabres</option>*/
/*                       <option value="Calgary">Calgary Flames</option>*/
/*                       <option value="Carolina">Carolina Hurricanes</option>*/
/*                       <option value="Chicago">Chicago Blackhawks</option>*/
/*                       <option value="Colorado">Colorado Avalanche</option>*/
/*                       <option value="Columbus">Columbus Blue Jackets</option>*/
/*                       <option value="Dallas">Dallas Stars</option>*/
/*                       <option value="Detroit">Detroit Red Wings</option>*/
/*                       <option value="Edmonton">Edmonton Oilers</option>*/
/*                       <option value="Florida">Florida Panthers</option>*/
/*                       <option value="Kings">Los Angeles Kings</option>*/
/*                       <option value="Minesota">Minnesota Wild</option>*/
/*                       <option value="Montreal">Montreal Canadiens</option>*/
/*                       <option value="Nashvile">Nashville Predators</option>*/
/*                       <option value="Jersey">New Jersey Devils</option>*/
/*                       <option value="Islanders">New York Islanders</option>*/
/*                       <option value="Rangers">New York Rangers</option>*/
/*                       <option value="Ottawa">Ottawa Senators</option>*/
/*                       <option value="Philadelphia">Philadelphia Flyers</option>*/
/*                       <option value="Pitsburgh">Pittsburgh Penguins</option>*/
/*                       <option value="Sharks">San Jose Sharks</option>*/
/*                       <option value="Blues">St Louis Blues</option>*/
/*                       <option value="Lightning">Tampa Bay Lightning</option>*/
/*                       <option value="Toronto">Toronto Maple Leafs</option>*/
/*                       <option value="Vancouver">Vancouver Canucks</option>*/
/*                       <option value="Washington">Washington Capitals</option>*/
/*                       <option value="Winnipeg">Winnipeg Jets</option>*/
/*                     </select> */
/*                 </div>*/
/*  #}*/
