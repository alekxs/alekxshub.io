<?php

/* regular.twig */
class __TwigTemplate_7dc70ab7f4b20006a908f28e3977dbb786bbc6f94eec65369216ab9614c7a20e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("_.twig", "regular.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "\t<div class=\"data\" id=\"team_stats_reg\">
\t\t<div class=\"team_stats_table\">
\t\t\t<h1>Team statistics</h1>
\t\t\t\t<table class=\"team_stats_list\">
\t\t\t\t\t";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["name"]) ? $context["name"] : null), 0, array(), "array")));
        foreach ($context['_seq'] as $context["_key"] => $context["keys"]) {
            // line 9
            echo "\t\t\t\t\t";
            if (($context["keys"] != "ID")) {
                // line 10
                echo "\t\t\t\t\t<th>";
                echo twig_escape_filter($this->env, $context["keys"], "html", null, true);
                echo "</th>
\t\t\t\t\t";
            }
            // line 12
            echo "\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['keys'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["name"]) ? $context["name"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
            // line 15
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["name"]) ? $context["name"] : null), $context["index"], array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
                // line 17
                echo "\t\t\t\t\t\t";
                if (($context["key"] != "ID")) {
                    // line 18
                    echo "\t\t\t\t\t\t<td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["name"]) ? $context["name"] : null), $context["index"], array(), "array"), $context["key"], array(), "array"), "html", null, true);
                    echo "</td>
\t\t\t\t\t\t";
                }
                // line 20
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "  
\t\t\t\t\t</tbody>                         
\t\t\t\t</table>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "regular.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 22,  85 => 21,  79 => 20,  73 => 18,  70 => 17,  66 => 16,  63 => 15,  59 => 14,  56 => 13,  50 => 12,  44 => 10,  41 => 9,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '_.twig' %}*/
/* */
/* {% block main %}*/
/* 	<div class="data" id="team_stats_reg">*/
/* 		<div class="team_stats_table">*/
/* 			<h1>Team statistics</h1>*/
/* 				<table class="team_stats_list">*/
/* 					{% for keys in name[0]|keys %}*/
/* 					{% if keys != "ID" %}*/
/* 					<th>{{keys}}</th>*/
/* 					{% endif %}*/
/* 					{% endfor %}*/
/* 					<tbody>*/
/* 						{% for index in name|keys %}*/
/* 						<tr>*/
/* 						{% for key in name[index]|keys %}*/
/* 						{% if key != "ID" %}*/
/* 						<td>{{name[index][key]}}</td>*/
/* 						{% endif %}*/
/* 						{% endfor %}*/
/* 						</tr>*/
/* 						{% endfor %}  */
/* 					</tbody>                         */
/* 				</table>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock main %}*/
/* */
/* {#  LEAGUE STANDINGS (VISAS DIVĪZIJAS DROPDOWN) LEAGUE LEADERS(DAUDZ TABULU, DROPDOWN) TEAM STATS     #}*/
