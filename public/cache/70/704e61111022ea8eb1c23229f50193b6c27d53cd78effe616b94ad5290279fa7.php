<?php

/* team_info.twig */
class __TwigTemplate_82fe7bee259a5566a981865ba7b4dcb079a821c9d2c098c8cbb0e1317e59b650 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("_.twig", "team_info.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"selected_team_info\" id=\"specific_team_data\">
<h1></h1>
  <div class=\"sort_buttons_specific_team\">
    <button>BASIC</button>
    <button>ADVANCED</button>
    <button>TOI</button>
    <button>GOALIE</button>
  </div>
";
        // line 13
        $context["specific_team_data"] = $this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), "ANA", array(), "array");
        // line 14
        echo "    <div id=\"maincontainer_big_table\">
      <div id=\"childcontainer_big_table\">

      ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 18
            echo "        <table class=\"team_stats_list\">
        ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute($this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), $context["data"], array(), "array"), 0, array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["indexs"]) {
                // line 20
                echo "          ";
                if (((($context["indexs"] != "Tm") && ($context["indexs"] != "Rk")) && ($context["indexs"] != "ID"))) {
                    // line 21
                    echo "            <th>";
                    echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_replace_filter($context["indexs"], array("_" => " "))), "html", null, true);
                    echo "</th>
          ";
                }
                // line 23
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indexs'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "          <tbody>
          ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), $context["data"], array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
                // line 26
                echo "            <tr>
              ";
                // line 27
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute($this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), $context["data"], array(), "array"), $context["index"], array(), "array")));
                foreach ($context['_seq'] as $context["_key"] => $context["indexs"]) {
                    // line 28
                    echo "              ";
                    if (((($context["indexs"] != "Tm") && ($context["indexs"] != "Rk")) && ($context["indexs"] != "ID"))) {
                        // line 29
                        echo "              <td>";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["specific_team_data"]) ? $context["specific_team_data"] : null), $context["data"], array(), "array"), $context["index"], array(), "array"), $context["indexs"], array(), "array"), "html", null, true);
                        echo "</td>
               ";
                    }
                    // line 31
                    echo "              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indexs'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 32
                echo "            </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "  
          </tbody>  
        </table> 
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "      </div>
    </div>
</div>  

";
    }

    public function getTemplateName()
    {
        return "team_info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 37,  111 => 33,  104 => 32,  98 => 31,  92 => 29,  89 => 28,  85 => 27,  82 => 26,  78 => 25,  75 => 24,  69 => 23,  63 => 21,  60 => 20,  56 => 19,  53 => 18,  49 => 17,  44 => 14,  42 => 13,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '_.twig' %}*/
/* */
/* {% block main %}*/
/* */
/* <div class="selected_team_info" id="specific_team_data">*/
/* <h1></h1>*/
/*   <div class="sort_buttons_specific_team">*/
/*     <button>BASIC</button>*/
/*     <button>ADVANCED</button>*/
/*     <button>TOI</button>*/
/*     <button>GOALIE</button>*/
/*   </div>*/
/* {% set specific_team_data = specific_team_data['ANA'] %}*/
/*     <div id="maincontainer_big_table">*/
/*       <div id="childcontainer_big_table">*/
/* */
/*       {% for data in specific_team_data|keys %}*/
/*         <table class="team_stats_list">*/
/*         {% for indexs in specific_team_data[data][0]|keys %}*/
/*           {% if indexs != "Tm" and indexs != "Rk" and indexs != "ID" %}*/
/*             <th>{{indexs|replace({'_':' '})|upper}}</th>*/
/*           {% endif %}*/
/*         {% endfor %}*/
/*           <tbody>*/
/*           {% for index in specific_team_data[data]|keys %}*/
/*             <tr>*/
/*               {% for indexs in specific_team_data[data][index]|keys %}*/
/*               {% if indexs != "Tm" and indexs != "Rk" and indexs != "ID"  %}*/
/*               <td>{{specific_team_data[data][index][indexs]}}</td>*/
/*                {% endif %}*/
/*               {% endfor %}*/
/*             </tr>*/
/*           {% endfor %}  */
/*           </tbody>  */
/*         </table> */
/*       {% endfor %}*/
/*       </div>*/
/*     </div>*/
/* </div>  */
/* */
/* {% endblock main %}*/
