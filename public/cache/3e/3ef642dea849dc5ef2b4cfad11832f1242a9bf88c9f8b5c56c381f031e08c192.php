<?php

/* table_view_template.twig */
class __TwigTemplate_ed258844b7513e60a65f7a903f8f88d7799e0d51da7f19f617bac0d5fd7251b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("_.twig", "table_view_template.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "               
";
        // line 5
        if (((isset($context["leaders"]) ? $context["leaders"] : null) == "true")) {
            // line 6
            echo "           \t<div id=\"container\">   
                 <div class=\"sort_buttons_leaders\">
                    <button class=\"filter\" data-filter=\".category-s\">SKATERS</button>
                    <button class=\"filter\" data-filter=\".category-g\">GOALIES</button>
                    <button class=\"filter\" data-filter=\".category-all\">ALL</button>
                 </div> 
                  <div class=\"container_main container sortable\" id=\"leaders_mixing\" >
\t\t\t\t\t<div class=\"columns leaders_container\">
\t\t\t\t\t\t\t";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["league_leaders"]) ? $context["league_leaders"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
                // line 15
                echo "\t\t\t\t\t\t\t<div class=\"scrollable_table_leaders mix ";
                if (($context["index"] <= 20)) {
                    echo "category-s";
                }
                echo " ";
                if (($context["index"] > 20)) {
                    echo "category-g";
                }
                echo " category-all\">
\t\t\t\t\t\t\t\t<div id=\"maincontainer\">
\t\t\t\t\t\t\t\t\t<div id=\"childcontainer\">
\t\t\t\t\t\t\t\t\t\t<table class=\"team_stats_list\">
\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t<th title=\"Position\">POS</th>
\t\t\t\t\t\t\t\t\t\t\t";
                // line 21
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute($this->getAttribute((isset($context["league_leaders"]) ? $context["league_leaders"] : null), $context["index"], array(), "array"), 0, array(), "array")));
                foreach ($context['_seq'] as $context["_key"] => $context["keys"]) {
                    // line 22
                    echo "\t\t\t\t\t\t\t\t\t\t\t<th title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["league_leaders_tooltip_array"]) ? $context["league_leaders_tooltip_array"] : null), $context["keys"], array(), "array"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_replace_filter($context["keys"], array("Tm" => "Team"))), "html", null, true);
                    echo "</th>\t
\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['keys'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 24
                echo "\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 26
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["league_leaders"]) ? $context["league_leaders"] : null), $context["index"], array(), "array")));
                foreach ($context['_seq'] as $context["_key"] => $context["indexs"]) {
                    // line 27
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 28
                    if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["league_leaders"]) ? $context["league_leaders"] : null), $context["indexs"], array(), "array"), $context["index"], array(), "array"), 2, array(), "array") == $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["league_leaders"]) ? $context["league_leaders"] : null), $context["indexs"], array(), "array"), ($context["index"] - 1), array(), "array"), 2, array(), "array"))) {
                        // line 29
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                        echo twig_escape_filter($this->env, ($context["indexs"] + 1), "html", null, true);
                        echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 31
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["league_leaders"]) ? $context["league_leaders"] : null), $context["indexs"], array(), "array"), $context["index"], array(), "array"), 2, array(), "array") != $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["league_leaders"]) ? $context["league_leaders"] : null), $context["indexs"], array(), "array"), ($context["index"] - 1), array(), "array"), 2, array(), "array"))) {
                        // line 32
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                        echo twig_escape_filter($this->env, ($context["indexs"] + 2), "html", null, true);
                        echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 34
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["league_leaders"]) ? $context["league_leaders"] : null), $context["index"], array(), "array"), $context["indexs"], array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["indexss"]) {
                        // line 35
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $context["indexss"]), "html", null, true);
                        echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indexss'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 36
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['indexs'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 38
                echo " 
\t\t\t\t\t\t\t\t\t\t\t\t</tbody> 
\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "  
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

";
        }
        // line 51
        if (((isset($context["leaders"]) ? $context["leaders"] : null) != "true")) {
            // line 52
            echo "<button class=\"table_reset\">Reset sorting</button>
\t<div class=\"data\" id=\"team_stats_reg\">
\t\t<div class=\"team_stats_table\">
\t\t\t<div class=\"scrollable_table\">
\t\t\t";
            // line 56
            if (((isset($context["count"]) ? $context["count"] : null) > 15)) {
                // line 57
                echo "\t\t\t\t<div id=\"maincontainer_big_table\">
\t\t\t\t\t\t
\t\t\t";
            }
            // line 60
            echo "\t\t\t";
            if (((isset($context["count"]) ? $context["count"] : null) <= 15)) {
                // line 61
                echo "\t\t\t\t<div id=\"maincontainer_small_table\">\t\t
\t\t\t";
            }
            // line 63
            echo "\t\t\t<input type=\"search\" class=\"light-table-filter fixed_header\" data-table=\"order-table\" placeholder=\"Search...\">
\t\t\t\t<table class=\"team_stats_list order-table sortable\" id=\"responsecontainer\">
\t\t\t\t\t<thead class=\"fixed_header\">
\t\t\t\t\t\t";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["team_stats"]) ? $context["team_stats"] : null), 0, array(), "array")));
            foreach ($context['_seq'] as $context["_key"] => $context["keys"]) {
                // line 67
                echo "\t\t\t\t\t\t";
                if ((($context["keys"] != "ID") && ($context["keys"] != "Rk"))) {
                    // line 68
                    echo "\t\t\t\t\t\t<th title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player_stats_tooltip_array"]) ? $context["player_stats_tooltip_array"] : null), $context["keys"], array(), "array"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_replace_filter($context["keys"], array("_" => " ")), "html", null, true);
                    echo "</th>
\t\t\t\t\t\t";
                }
                // line 70
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['keys'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t";
            // line 73
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["team_stats"]) ? $context["team_stats"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
                // line 74
                echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t";
                // line 75
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["team_stats"]) ? $context["team_stats"] : null), $context["index"], array(), "array")));
                foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
                    // line 76
                    echo "\t\t\t\t\t\t\t";
                    if ((($context["key"] != "ID") && ($context["key"] != "Rk"))) {
                        // line 77
                        echo "\t\t\t\t\t\t\t<td>";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["team_stats"]) ? $context["team_stats"] : null), $context["index"], array(), "array"), $context["key"], array(), "array"), "html", null, true);
                        echo "</td>
\t\t\t\t\t\t\t";
                    }
                    // line 79
                    echo "\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
                // line 81
                if (((($context["index"] % 30) == 0) && ($context["index"] != 0))) {
                    // line 82
                    echo "\t\t\t\t\t<tr class=\"info_header\">
\t\t\t\t\t\t";
                    // line 83
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute((isset($context["team_stats"]) ? $context["team_stats"] : null), 0, array(), "array")));
                    foreach ($context['_seq'] as $context["_key"] => $context["keys"]) {
                        // line 84
                        echo "\t\t\t\t\t\t";
                        if ((($context["keys"] != "ID") && ($context["keys"] != "Rk"))) {
                            // line 85
                            echo "\t\t\t\t\t\t<td>";
                            echo twig_escape_filter($this->env, twig_replace_filter($context["keys"], array("_" => " ")), "html", null, true);
                            echo "</td>
\t\t\t\t\t\t";
                        }
                        // line 87
                        echo "\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['keys'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 88
                    echo "\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
                }
                // line 90
                echo "\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "  
\t\t\t\t\t\t</tbody>                         
\t\t\t\t</table>
\t\t\t\t
\t\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>
";
        }
        // line 99
        echo "
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "table_view_template.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 99,  261 => 90,  257 => 88,  251 => 87,  245 => 85,  242 => 84,  238 => 83,  235 => 82,  233 => 81,  230 => 80,  224 => 79,  218 => 77,  215 => 76,  211 => 75,  208 => 74,  204 => 73,  200 => 71,  194 => 70,  186 => 68,  183 => 67,  179 => 66,  174 => 63,  170 => 61,  167 => 60,  162 => 57,  160 => 56,  154 => 52,  152 => 51,  144 => 45,  131 => 38,  123 => 36,  114 => 35,  109 => 34,  103 => 32,  100 => 31,  94 => 29,  92 => 28,  89 => 27,  85 => 26,  81 => 24,  70 => 22,  66 => 21,  50 => 15,  46 => 14,  36 => 6,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '_.twig' %}*/
/* */
/* {% block main %}*/
/*                */
/* {% if leaders == "true" %}*/
/*            	<div id="container">   */
/*                  <div class="sort_buttons_leaders">*/
/*                     <button class="filter" data-filter=".category-s">SKATERS</button>*/
/*                     <button class="filter" data-filter=".category-g">GOALIES</button>*/
/*                     <button class="filter" data-filter=".category-all">ALL</button>*/
/*                  </div> */
/*                   <div class="container_main container sortable" id="leaders_mixing" >*/
/* 					<div class="columns leaders_container">*/
/* 							{% for index in league_leaders|keys %}*/
/* 							<div class="scrollable_table_leaders mix {% if index <= 20 %}category-s{% endif %} {% if index > 20 %}category-g{% endif %} category-all">*/
/* 								<div id="maincontainer">*/
/* 									<div id="childcontainer">*/
/* 										<table class="team_stats_list">*/
/* 										<thead>*/
/* 											<th title="Position">POS</th>*/
/* 											{% for keys in league_leaders[index][0]|keys %}*/
/* 											<th title="{{league_leaders_tooltip_array[keys]}}">{{keys|replace({"Tm": "Team"})|upper}}</th>	*/
/* 											{% endfor %}*/
/* 										</thead>*/
/* 												<tbody>*/
/* 													{% for indexs in league_leaders[index]|keys %}*/
/* 														<tr>*/
/* 														{% if league_leaders[indexs][index][2] == league_leaders[indexs][index-1][2] %}*/
/* 														<td>{{indexs+1}}</td>*/
/* 														{% endif %}*/
/* 														{% if league_leaders[indexs][index][2] != league_leaders[indexs][index-1][2]  %}*/
/* 														<td>{{indexs+2}}</td>*/
/* 														{% endif %}*/
/* 															{% for indexss in league_leaders[index][indexs] %}*/
/* 																<td>{{indexss|upper}}</td>*/
/* 															{% endfor %} */
/* 														</tr>*/
/* 													{% endfor %} */
/* 												</tbody> */
/* 										</table>*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* */
/* 							{% endfor %}  */
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* */
/* {% endif %}*/
/* {% if leaders != "true" %}*/
/* <button class="table_reset">Reset sorting</button>*/
/* 	<div class="data" id="team_stats_reg">*/
/* 		<div class="team_stats_table">*/
/* 			<div class="scrollable_table">*/
/* 			{% if count > 15 %}*/
/* 				<div id="maincontainer_big_table">*/
/* 						*/
/* 			{% endif %}*/
/* 			{% if count <= 15 %}*/
/* 				<div id="maincontainer_small_table">		*/
/* 			{% endif %}*/
/* 			<input type="search" class="light-table-filter fixed_header" data-table="order-table" placeholder="Search...">*/
/* 				<table class="team_stats_list order-table sortable" id="responsecontainer">*/
/* 					<thead class="fixed_header">*/
/* 						{% for keys in team_stats[0]|keys %}*/
/* 						{% if keys != "ID" and keys != "Rk" %}*/
/* 						<th title="{{player_stats_tooltip_array[keys]}}">{{keys|replace({'_':' '})}}</th>*/
/* 						{% endif %}*/
/* 						{% endfor %}*/
/* 					</thead>*/
/* 						<tbody>*/
/* 							{% for index in team_stats|keys %}*/
/* 							<tr>*/
/* 							{% for key in team_stats[index]|keys %}*/
/* 							{% if key != "ID" and key != "Rk" %}*/
/* 							<td>{{team_stats[index][key]}}</td>*/
/* 							{% endif %}*/
/* 							{% endfor %}*/
/* 							</tr>*/
/* 							{% if index % 30 == 0 and index != 0 %}*/
/* 					<tr class="info_header">*/
/* 						{% for keys in team_stats[0]|keys %}*/
/* 						{% if keys != "ID" and keys != "Rk" %}*/
/* 						<td>{{keys|replace({'_':' '})}}</td>*/
/* 						{% endif %}*/
/* 						{% endfor %}*/
/* 					</tr>*/
/* 							{% endif %}*/
/* 							{% endfor %}  */
/* 						</tbody>                         */
/* 				</table>*/
/* 				*/
/* 				</div>*/
/* 			*/
/* 		</div>*/
/* 	</div>*/
/* {% endif %}*/
/* */
/*     </div>*/
/* </div>*/
/* */
/* {% endblock main %}*/
/* */
