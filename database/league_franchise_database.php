<?php
function league_franchise_table_creator($data_array){
$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';

$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

//Izveido tabulu
	$sql = "CREATE TABLE IF NOT EXISTS `active_franchises` (
	`Franchise` varchar(30) NOT NULL,
	`Lg` char(3) NOT NULL,
	`From` int(4) NOT NULL,
	`To` int(4) NOT NULL,
	`Yrs` int(3) NOT NULL,
	`GP` int(5) NOT NULL,
	`W` int(4) NOT NULL,
	`L` int(4) NOT NULL,
	`T` int(4) NOT NULL,
	`OL` int(4) NOT NULL,
	`PTS` int(5) NOT NULL,
	`PTS%` float(3) NOT NULL,
	`Yrs Plyf` int(3) NOT NULL,
	`Div` int(2) NOT NULL,
	`Conf`  int(2) NOT NULL,
	`Champ`  int(2) NOT NULL,
	`St Cup`  int(2) NOT NULL,
	PRIMARY KEY (`Franchise`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$sql = trim($sql);
	$pdo->exec($sql);

//Izdzes tabulas datus, pirms ierakstit jaunus
	if(true){
		$deleteStatement = $pdo->delete()
                           ->from('`active_franchises`');
		$affectedRows = $deleteStatement->execute(false);
	}
//Ieraksta datus tabulaa
for( $i=0; $i<30; $i++){
	$insertStatement = $pdo->insert(['`Franchise`','`Lg`','`From`','`To`','`Yrs`','`GP`','`W`','`L`','`T`','`OL`','`PTS`','`PTS%`','`Yrs Plyf`','`Div`','`Conf`','`Champ`','`St Cup`'])
	                       ->into('`active_franchises`')
	                       ->values($data_array[$i]);
	$insertStatement->execute(false);
	}
}







