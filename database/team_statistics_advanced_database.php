<?php
function team_statistics_advanced_creator($data_array){
$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';

$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

//Izveido tabulu
	$sql = "CREATE TABLE IF NOT EXISTS `advanced_team_statistics` (
	`Rk` int(2) NOT NULL,
	`Team` varchar(30) NOT NULL,
	`CF` int(4) NOT NULL,
	`CA` int(4) NOT NULL,
	`CF%` float(3) NOT NULL,
	`FF` int(4) NOT NULL,
	`FA` int(4) NOT NULL,
	`FF%` float(3) NOT NULL,
	`oZS%` float(3) NOT NULL,
	`dZS%` float(3) NOT NULL,
	`HIT` int(4) NOT NULL,
	`BLK` int(4) NOT NULL,
	`FOwin` int(4) NOT NULL,
	`FOloss` int(4) NOT NULL,
	`FO%` float(3) NOT NULL,
	PRIMARY  KEY (`Team`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$sql = trim($sql);
	$pdo->exec($sql);

//Izdzes tabulas datus, pirms ierakstit jaunus
	if(true){
		$deleteStatement = $pdo->delete()
                           ->from('`advanced_team_statistics`');
		$affectedRows = $deleteStatement->execute(false);
	}
//ieraksta datus tabulaa
for( $i=0; $i<sizeof($data_array); $i++){
	$insertStatement = $pdo->insert([ 'Rk','Team','CF','CA','`CF%`','FF','FA','`FF%`','`oZS%`','`dZS%`','HIT','BLK','FOwin','FOloss','`FO%`'])
	                       ->into('`advanced_team_statistics`')
	                       ->values($data_array[$i]);
	$insertStatement->execute(false);
	}
}







