<?php
function skaters_advanced_creator($data_array, $season_statuss){
$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';
if($season_statuss=="regular"){
$table_name='`skaters_advanced_statistics_regular`';
}
if($season_statuss=="playoff"){
$table_name='`skaters_advanced_statistics_playoff`';
}
$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

//Izveido tabulu
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	`Rk` int(2) NOT NULL,
	`Player` varchar(40) NOT NULL,
	`Age` int(2) NOT NULL,
	`Tm` varchar(3) NOT NULL,
	`Pos` varchar(2) NOT NULL,
	`GP` int(3) NOT NULL,
	`CF` int(4) NOT NULL,
	`CA` int(4) NOT NULL,
	`CF%` float(3) NOT NULL,
	`CF% rel` float(3) NOT NULL,
	`C/60` float(3) NOT NULL,
	`Crel/60` float(3) NOT NULL,
	`FF` int(4) NOT NULL,
	`FA` int(4) NOT NULL,
	`FF%` float(3) NOT NULL,
	`FF% rel` float(3) NOT NULL,
	`oiSH%` float(3) NOT NULL,
	`oiSV%` float(3) NOT NULL,
	`PDO` float(4) NOT NULL,
	`oZS%` float(3) NOT NULL,
	`dZS%` float(3) NOT NULL,
	`TOI/60` time NOT NULL,
	`TOI(EV)` time NOT NULL,
	`TK` int(3) NOT NULL,
	`GV` int(3) NOT NULL,
	`E+/-` float(3) NOT NULL,
	`SAtt.` int(3) NOT NULL,
	`Thru%` float(3) NOT NULL,
	PRIMARY  KEY (`Player`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$sql = trim($sql);
	$pdo->exec($sql);

//Izdzes tabulas datus, pirms ierakstit jaunus
	if($table_name){
		$deleteStatement = $pdo->delete()
                           ->from($table_name);
		$affectedRows = $deleteStatement->execute(false);
	}
//ieraksta datus tabulaa
for( $i=0; $i<sizeof($data_array); $i++){
	
	$insertStatement = $pdo->insert([ 'Rk','Player','Age','Tm','Pos','GP','CF','CA','`CF%`','`CF% rel`','`C/60`','`Crel/60`','FF','FA','`FF%`','`FF% rel`','`oiSH%`','`oiSV%`','PDO','`oZS%`','`dZS%`','`TOI/60`','`TOI(EV)`','TK','GV','`E+/-`','`SAtt.`','`Thru%`'])
	                       ->into($table_name)
	                       ->values($data_array[$i]);
	$insertStatement->execute(false);
	}
}

	