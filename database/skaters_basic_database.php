<?php
function skaters_basic_creator($data_array, $season_statuss){
$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';

if($season_statuss=="regular"){
$table_name='`skaters_basic_statistics_regular`';
}
if($season_statuss=="playoff"){
$table_name='`skaters_basic_statistics_playoff`';
}
$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

//Izveido tabulu
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	`Rk` int(2) NOT NULL,
	`Player` varchar(40) NOT NULL,
	`Age` int(2) NOT NULL,
	`Tm` varchar(3) NOT NULL,
	`Pos` varchar(2) NOT NULL,
	`GP` int(2) NOT NULL,
	`G` int(2) NOT NULL,
	`A` int(2) NOT NULL,
	`PTS` int(3) NOT NULL,
	`+/-` int(2) NOT NULL,
	`PIM` int(3) NOT NULL,
	`EVG` int(2) NOT NULL,
	`PPG` int(2) NOT NULL,
	`SHG` int(2) NOT NULL,
	`GW` int(2) NOT NULL,
	`EVA` int(2) NOT NULL,
	`PPA` int(2) NOT NULL,
	`SHA` int(2) NOT NULL,
	`S` int(4) NOT NULL,
	`S%` float(3) NOT NULL,
	`TOI` int(4) NOT NULL,
	`ATOI` time NOT NULL,
	`BLK` int(3) NOT NULL,
	`HIT` int(3) NOT NULL,
	`FOwin` int(3) NOT NULL,
	`FOloss` int(4) NOT NULL,
	`FO%` float(3) NOT NULL,
	PRIMARY  KEY (`Player`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$sql = trim($sql);
	$pdo->exec($sql);

//Izdzes tabulas datus, pirms ierakstit jaunus
	if($table_name){
		$deleteStatement = $pdo->delete()
                           ->from($table_name);
		$affectedRows = $deleteStatement->execute(false);
	}
//Ieraksta datus tabulaa
for( $i=0; $i<sizeof($data_array); $i++){
	
	$insertStatement = $pdo->insert([ 'Rk','Player','Age','Tm','Pos','GP','G','A','PTS','`+/-`','PIM','EVG','PPG',"SHG","GW",'EVA','PPA','SHA','S','`S%`','TOI','ATOI','BLK','HIT','FOwin','FOloss','`FO%`'])
	                       ->into($table_name)
	                       ->values($data_array[$i]);
	$insertStatement->execute(false);
	}
}







