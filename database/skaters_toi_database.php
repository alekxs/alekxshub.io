<?php
function skaters_toi_creator($data_array, $season_statuss){
$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';

$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

if($season_statuss=="regular"){
$table_name='`skaters_toi_statistics_regular`';
}
if($season_statuss=="playoff"){
$table_name='`skaters_toi_statistics_playoff`';
}
$table_name;
//Izveido tabulu
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	`Rk` int(2) NOT NULL,
	`Player` varchar(40) NOT NULL,
	`Tm` varchar(3) NOT NULL,
	`POS` varchar(2) NOT NULL,
	`Shift` time NOT NULL,
	`GP` int(2) NOT NULL,
	`TOI ES` time NOT NULL,
	`CF% Rel ES` float(3) NOT NULL,
	`GF/60 ES` float(3) NOT NULL,
	`GA/60 ES` float(3) NOT NULL,
	`TOI PP` time NOT NULL,
	`CF% Rel PP` float(3) NOT NULL,
	`GF/60 PP` float(4) NOT NULL,
	`GA/60 PP` float(2) NOT NULL,
	`TOI SH` time NOT NULL,
	`CF% Rel SH` float(4) NOT NULL,
	`GF/60 SH` float(3) NOT NULL,
	`GA/60 SH` float(4) NOT NULL,
	PRIMARY  KEY (`Player`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$sql = trim($sql);
	$pdo->exec($sql);

//Izdzes tabulas datus, pirms ierakstit jaunus
	if($table_name){
		$deleteStatement = $pdo->delete()
                           ->from($table_name);
		$affectedRows = $deleteStatement->execute(false);
	}
//Ieraksta datus tabulaa
for( $i=0; $i<sizeof($data_array); $i++){
	
	$insertStatement = $pdo->insert([ 'Rk','Player','Tm','POS','Shift','GP','`TOI ES`','`CF% Rel ES`','`GF/60 ES`','`GA/60 ES`','`TOI PP`','`CF% Rel PP`','`GF/60 PP`','`GA/60 PP`','`TOI SH`','`CF% Rel SH`','`GF/60 SH`','`GA/60 SH`'])
	                       ->into($table_name)
	                       ->values($data_array[$i]);
	$insertStatement->execute(false);
	}
}







