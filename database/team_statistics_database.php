<?php
function team_statistics_creator($data_array){
$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';

$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

//Izveido tabulu
	$sql = "CREATE TABLE IF NOT EXISTS `team_statistics` (
	`Rk` int(2) NOT NULL,
	`Team` varchar(30) NOT NULL,
	`AvAge` float(3) NOT NULL,
	`GP` int(2) NOT NULL,
	`W` int(2) NOT NULL,
	`L` int(2) NOT NULL,
	`OL` int(2) NOT NULL,
	`PTS` int(3) NOT NULL,
	`PTS%` float(4) NOT NULL,
	`GF` int(3) NOT NULL,
	`GA` int(3) NOT NULL,
	`SRS` float(4) NOT NULL,
	`SOS` float(4) NOT NULL,
	`TG/G` float(4) NOT NULL,
	`PP` int(2) NOT NULL,
	`PPO` int(3) NOT NULL,
	`PP%` float(4) NOT NULL,
	`PPA` int(2) NOT NULL,
	`PPOA` int(3) NOT NULL,
	`PK%` float(4) NOT NULL,
	`SH` int(2) NOT NULL,
	`SHA` int(2) NOT NULL,
	`S` int(4) NOT NULL,
	`S%` float(2) NOT NULL,
	`SA` int(4) NOT NULL,
	`SV%` float(4) NOT NULL,
	`PDO` float(4) NOT NULL,
	PRIMARY  KEY (`Team`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$sql = trim($sql);
	$pdo->exec($sql);

//Izdzes tabulas datus, pirms ierakstit jaunus
	if(true){
		$deleteStatement = $pdo->delete()
                           ->from('`team_statistics`');
		$affectedRows = $deleteStatement->execute(false);
	}
//Ieraksta datus tabulaa
for( $i=0; $i<sizeof($data_array); $i++){
	
	$insertStatement = $pdo->insert([ 'Rk','Team','AvAge','GP','W','L','OL','PTS','`PTS%`','GF','GA','SRS','SOS','`TG/G`','PP','PPO','`PP%`','PPA','PPOA','`PK%`','SH','SHA','S','`S%`','SA','`SV%`','PDO'])
	                       ->into('`team_statistics`')
	                       ->values($data_array[$i]);
	$insertStatement->execute(false);
	}

}







