<?php
function goalie_statistics__creator_playoff($data_array){
$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';

$table_name='`goalie_statistics_playoff`';

$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);
//Izveido tabulu
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	`Rk` int(2) NOT NULL,
	`Player` varchar(40) NOT NULL,
	`Age` int(2) NOT NULL,
	`Tm` varchar(3) NOT NULL,
	`GP` int(3) NOT NULL,
	`GS` int(2) NOT NULL,
	`W` int(2) NOT NULL,
	`L` int(2) NOT NULL,
	`GA` int(3) NOT NULL,
	`SA` int(4) NOT NULL,
	`SV` int(4) NOT NULL,
	`SV%` float(4) NOT NULL,
	`GAA` float(4) NOT NULL,
	`SO` int(1) NOT NULL,
	`MIN` int(4) NOT NULL,
	`QS` int(2) NOT NULL,
	`QS%` float(3) NOT NULL,
	`RBS` int(2) NOT NULL,
	`GA%-` int(3) NOT NULL,
	`GSAA` float(3) NOT NULL,
	`G` int(2) NOT NULL,
	`A` int(2) NOT NULL,
	`PTS` int(2) NOT NULL,
	`PIM` int(2) NOT NULL,
	PRIMARY KEY (`Player`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$sql = trim($sql);
	$pdo->exec($sql);

	//Izdzes tabulas datus, pirms ierakstit jaunus
	if($table_name){
		$deleteStatement = $pdo->delete()
                           ->from($table_name);
		$affectedRows = $deleteStatement->execute(false);
	}
//Ieraksta datus tabulaa
for( $i=0; $i<sizeof($data_array); $i++){
	$insertStatement = $pdo->insert([ 'Rk','Player','Age','Tm','GP','GS','W','L','GA','SA','SV','`SV%`','GAA','SO','MIN','QS','`QS%`','RBS','`GA%-`','GSAA','G','A','PTS','PIM'])
	                       ->into($table_name)
	                       ->values($data_array[$i]);
	$insertStatement->execute(false);
	}
}







