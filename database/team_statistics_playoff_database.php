<?php
function team_statistics_playoff_creator($data_array){
$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';

$table_name='`team_playoff_statistics`';

$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

//Izveido tabulu
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	`Rk` int(2) NOT NULL,
	`Team` varchar(30) NOT NULL,
	`GP` int(2) NOT NULL,
	`W` int(2) NOT NULL,
	`L` int(2) NOT NULL,
	`T` int(2) NOT NULL,
	`OW` int(2) NOT NULL,
	`OL` int(2) NOT NULL,
	`W-L%` float(4) NOT NULL,
	`G` int(2) NOT NULL,
	`GA` int(2) NOT NULL,
	`DIFF` int(2) NOT NULL,
	PRIMARY  KEY (`Team`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$sql = trim($sql);
	$pdo->exec($sql);

//Izdzes tabulas datus, pirms ierakstit jaunus
	if($table_name){
		$deleteStatement = $pdo->delete()
                           ->from($table_name);
		$affectedRows = $deleteStatement->execute(false);
	}

//ieraksta datus tabulaa
for( $i=0; $i<sizeof($data_array); $i++){
	$insertStatement = $pdo->insert([ 'Rk','Team','GP','W','L','T','OW','OL','`W-L%`','G','GA','DIFF'])
	                       ->into($table_name)
	                       ->values($data_array[$i]);
	$insertStatement->execute(false);
	}

}







