<?php

//Funkcija, kas veic visu tabulu datu atlasi no konkretas tabulas
function return_regular_season_array($table_name){

$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';
$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

$selectStatement = $pdo->select()->from($table_name);
$stmt = $selectStatement->execute();
$data = $stmt->fetchAll();

return $data;
	
}

//Funkcija, kas veic diviziju datu atlasi
function return_division_array($table_name){

$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';
$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

$divisions_id=["ad", "pd", "md", "cd" ];
for($i=0; $i<4; $i++){
$selectStatement = $pdo->select()->from($table_name)->where('ID', '=' , $divisions_id[$i]);
$stmt = $selectStatement->execute();
$data[$i] = $stmt->fetchAll();
	}
	return $data;
}

//Datu atlase konkretai tabulai, veic atlasi no 6 tabulaam
function specific_team_data($team_name){

$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';
$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

$table_names=["`skaters_basic_statistics_regular`","`skaters_advanced_statistics_regular`","`skaters_toi_statistics_regular`","`goalie_statistics_regular`","`team_statistics`","`advanced_team_statistics`"];

$team_short_names=["ANA" => "Anaheim Ducks", "ARI"=> "Arizona Coyotes", "BOS"=> "Boston Bruins", "BUF"=> "Buffalo Sabres", "CAR"=> "Carolina Hurricanes", "CBJ"=> "Columbus Blue Jackets", "CGY"=> "Calgary Flames",
"CHI" => "Chicago Blackhawks", "COL" => "Colorado Avalanche", "DAL" => "Dallas Stars", "DET" => "Detroit Red Wings", 
"EDM" => "Edmonton Oilers", "FLA" => "Florida Panthers", "LAK" => "Los Angeles Kings", "MIN" => "Minnesota Wild", "MTL" => "Montreal Canadiens", "NJD" => "New Jersey Devils", "NSH" => "Nashville Predators",
"NYI" => "New York Islanders", "NYR" => "New York Rangers", "OTT" => "Ottawa Senators", "PHI" => "Philadelphia Flyers", 
"PIT" => "Pittsburgh Penguins", "SJS" => "San Jose Sharks", "STL" => "St. Louis Blues", "TBL" => "Tampa Bay Lightning", "TOR" => "Toronto Maple Leafs", "VAN" => "Vancouver Canucks", "WPG" => "Winnipeg Jets", "WSH" => "Washington Capitals"];

	for($i=0; $i<6; $i++){
		if($i<4){
$selectStatement = $pdo->select()->from($table_names[$i])->where('Tm', '=' , $team_name);	
		}
		else{
$selectStatement = $pdo->select()->from($table_names[$i])->where('Team', '=' , $team_short_names[$team_name]);
		}
$stmt = $selectStatement->execute();
$data[$i] = $stmt->fetchAll();
	}
return $data;
}

//Izveido ligas liderus atkariba no sezonas statusa
function league_leaders_creator($season_statuss){

$dsn = 'mysql:host=127.0.0.1;dbname=nhl;charset=utf8';
$usr = 'root';
$pwd = '';
$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

if( $season_statuss == "regular"){
	$table_name_skaters = "skaters_basic_statistics_regular";
	$table_name_goalies = "goalie_statistics_regular";
	$league_leaders_table_headings_goalies = [ "`GP`", "`W`", "`L`", "`T/O`", "`GA`", "`SA`", "`SV`", "`SV%`", "`GAA`", "`SO`", "`MIN`", "`PTS`", "`PIM`" ];
}
else{
	$table_name_skaters = "skaters_basic_statistics_playoff";
	$table_name_goalies = "goalie_statistics_playoff";
	$league_leaders_table_headings_goalies = [ "`GP`", "`W`", "`L`", "`GA`", "`SA`", "`SV`", "`SV%`", "`GAA`", "`SO`", "`MIN`", "`PTS`", "`PIM`" ];
}

$league_leaders_table_headings_skaters = [ "`G`", "`A`", "`PTS`", "`+/-`", "`PIM`", "`EVG`", "`PPG`", "`SHG`", "`GW`", "`EVA`", "`PPA`", "`SHA`", "`S`", "`S%`", "`TOI`", "`ATOI`", "`BLK`", "`HIT`", "`FOwin`", "`FOloss`", "`FO%`" ];

for( $counter = 0; $counter<count($league_leaders_table_headings_skaters); $counter++){
	$selectStatement = $pdo->select( [ "Player","Tm", $league_leaders_table_headings_skaters[$counter] ] )->from( $table_name_skaters )->orderBy( $league_leaders_table_headings_skaters[$counter], 'DESC' );
	$stmt = $selectStatement->limit(20)->execute();
	$data[$counter] = $stmt->fetchAll();
}

for( $counter = count($league_leaders_table_headings_skaters), $index=0;( $counter < count($league_leaders_table_headings_skaters) + count($league_leaders_table_headings_goalies) ); $counter++, $index++){
	$selectStatement = $pdo->select( [ "Player","Tm", $league_leaders_table_headings_goalies[$index] ] )->from( $table_name_goalies )->orderBy( $league_leaders_table_headings_goalies[$index], 'DESC' );
	$stmt = $selectStatement->limit(20)->execute();
	$data[$counter] = $stmt->fetchAll();
}

return $data;
	
}
//Seit kolonnu paskaidrosanai tiek izveidotas funkcijas, kas atgriez asociativu masivu ar skaidrojumiem. 
function leaders_tooltip_creator(){

	$tooltip_array = [ "G" => "Goals", "A" => "Assists", "PTS" => "Points", "+/-" => "Plus/Minus", "PIM" => "Penalties in Minutes", "EVG" => "Even Strenght Goals", "PPG" => "Power Play Goals", 
	"SHG" => "Short-Handed Goals", "GW" => "Game-Winning Goals", "EVA" => "Even-Strength Assists", "PPA" => "Power Play Assists", "SHA" => "Short-Handed Assists", "S" => "Shots on Goal", "S%" => "Shooting Percentage", "TOI" => "Time on Ice (in minutes)",
	"ATOI" => "Average Time on Ice", "BLK" => "Blocks at Even Strength", "HIT" => "Hits at Even Strength", "FOwin" => "Faceoff Wins at Even Strength", "FOloss" => "Faceoff Losses at Even Strength", "FO%" => "Faceoff Win Percentage at Even Strength", "GP" => "Games Played", "W" => "Wins",
	"L" => "Losses", "T/O" => "Ties plus Overtime/Shootout Losses", "GA" => "Goals Against", "SA" => "Shots Against", "SV" => "Saves", "SV%" => "Save Percentage", "GAA" => "Goals Against Average", "SO" => "Shutouts",
	"MIN" => "Minutes", "PIM" => "Penalties in Minutes" ];

	return $tooltip_array;

}

function league_standing_tooltip_creator(){

	$tooltip_array = [ "GP" => "Games Played", "W" => "Wins", "L" => "Losses", "OL" => "Overtime/Shootout Losses", "PTS" => "Points", 
	"PTS%" => "Points Percentage", "GF" => "Goals For", "GA" => "Goals Against", "SRS" => "Simple Raiting System", "SOS" => "Strength of Schedule",
	 "RPts.%" => "Points Percentage","ROW" => "Wins in Regulation or Overtime"];

	return $tooltip_array;

}
//te jaasadrukaa visi paareejiee toooltttiippii
function basic_team_stats_tooltip_creator(){

	$tooltip_array = [ "Tm" => "Team","Pos" => "Position","GP" => "Games Played","G" => "Goals","A" => "Assists","PTS" => "Points","+/-" => "Plus/Minus",
	"PIM" => "Penalties in Minutes","EVG" => "Even Strength Goals","PPG" => "Power Play Assists","SHG" => "Short-Handed Assists","GW" => "Game-Winning Goals",
	"EVA" => "Even Strength Assists","PPA" => "Power Play Assists","SHA" => "Short-Handed Assists","S" => "Shots on Goal","S%" => "Shooting Percentage",
	"TOI" => "Time on Ice (in minutes)","ATOI" => "Average Time on Ice (in minutes)","BLK" => "Blocks at Even Strength","HIT" => "Hits at Even Strength",
	"FOwin" => "Faceoff Wins at Even Strength","Faceoff Losses at Even Strength" => "Goals","FO%" => "Faceoff Win Percentage at Even Strength","GS"=>"Games Started",
	"W"=>"Wins","L"=>"Losses","T/O"=>"Ties plus Overtime/Shootout Losses","GA" => "Goals Against", "SA" => "Shots Against","SV" => "Saves","SV%" => "Save Percentage",
	"GAA" => "Goals Against Average", "SO" => "Shutouts","MIN" => "Minutes", "QS"=>"Quality Starts","QS%"=> "Quality Starts Percentage (QS/GS)", "RBS"=>"Really Bad Starts",
	"GA%-"=>"Goals allowed % relative to league goals allowed", "GSAA"=> "Goals Saved Above Average","FOloss" => "Faceoff Losses at Even Strength",
	"AvAge"=>"Average Age", "OL"=>"Overtime Losses", "PTS%" => "Points Percentage","GF" => "Goals For","SRS" => "Simple Raiting System", "SOS" => "Strength of Schedule",
	"TG/G"=>"Total goals per game", "PP"=>"Power Play Goals", "PPO"=>"Power Play Opportunities","PP%"=>"Power Play Percentage", "PPOA"=>"Power Play Opportunities Against",
	"PK%"=>"Penalty Killing Percentage", "SH"=>"Short-Handed Goals","PDO"=>"PDO at Even Strength", "CF"=>"Corsi For in All Situations", "CA"=>"Corsi Against in All Situations",
	"CF%"=>"Corsi For % in All Situations (CF / (CF + CA))", "FF"=>"Fenwick For in All Situations", "FA"=>"Fenwick Against in All Situations",
	"FF%"=>"Fenwick For % in All Situations (FF / (FF + FA) )", "oZS%"=>"Offensive Zone Start % in All Situations (Offensive Zone Faceoffs / (Offensive Zone Faceoffs + Defensive Zone Faceoffs))",
	"dZS%"=> "Defensive Zone Start % in All Situations (Defensive Zone Faceoffs / (Offensive Zone Faceoffs + Defensive Zone Faceoffs))",
	"CF% rel"=>"Relative Corsi For % at Even Strength (CF% - CFoff%<br>On-Ice Corsi For % - Off-Ice Corsi For % )", "C/60"=>"Corsi per 60 Minutes at Even Strength<br />(CF - CA) * 60 / TOI",
	"Crel/60"=>"Relative Corsi per 60 Minutes at Even Strength (CF/60 - CFoff/60 On-Ice Corsi For/60 minutes - Off-Ice Corsi For/60 minutes)",
	"FF% rel"=> "Relative Fenwick For % at Even Strength (FF% - FFoff%<br>On-Ice Fenwick For % - Off-Ice Fenwick For %)",
	"oiSH%"=>"Team On-Ice Shooting Percentage at Even Strength (Shooting % while this player/team was on the ice)", "oiSV%"=>"Team On-Ice Save Percentage at Even Strength (Save % while this player/team was on the ice)",
	"TOI/60"=>"TOI/60 in All Situations<br />Time on Ice per 60 minutes", "TOI(EV)"=>"TOI/60 at Even Strength (Time on Ice per 60 minutes)",
	"TK"=>"Takeaways", "GV"=>"Giveaways", "E+/-"=>"Expected +/-' given where shots came from, for and against, while this player was on the ice at even strength.",
	"SAtt."=>"Total shots attempted in all situations", "Thru%"=>"Percentage of shots taken that go on net.", "Shift"=> "Avg. Shift Length/Gm",
	"TOI ES"=>"Avg. Time on Ice/Gm at Even Strength", "CF% Rel ES"=>"Relative Corsi For % at Even Strength", 
	"GF/60 ES"=> "On-Ice Goals For/60 Minutes at Even Strength", "GA/60 ES"=>"On-Ice Goals Against/60 Minutes at Even Strength",
	"TOI PP"=> "Avg. Time on Ice/Gm while on Power Play", "CF% Rel PP"=> "Relative Corsi For % on the Power Play",
	"GF/60 PP"=> "On-Ice Goals For/60 Minutes on the Power Play", "GA/60 PP"=> "On-Ice Goals Against/60 Minutes on the Power Play",
	"TOI SH"=> "Avg. Time on Ice/Gm while Short Handed", "CF% Rel SH"=> "Relative Corsi For % while Short Handed",
	"GF/60 SH"=>"On-Ice Goals For/60 Minutes while Short Handed", "GA/60 SH"=>"On-Ice Goals Against/60 Minutes while Short Handed"
	];

	return $tooltip_array;

}


